/**
 *
 * @author Yefersson Fajardo Gonzalez
 *
 *
 */
package co.edu.sena.miscelanea;

import static java.lang.Math.sqrt;
import java.util.Random;
import java.util.Scanner;


public class MetodosMiscelanea {
  
   public void Ejercicio1(){
     
    Scanner entrada = new Scanner(System.in);
    double base;
    double altura;
    double areaTriangulo;
   
    //Proceso
    System.out.println("1. Código java que permita calcular el área de un triángulo.");
    System.out.println(" ");
    System.out.println("Ingrese el valor de la Base");
    base = entrada.nextDouble();
    System.out.println("Ingrese el valor de la Altura");
    altura = entrada.nextDouble();
    
    areaTriangulo = (base * altura)/2;

    //Salidas
    
    System.out.printf("%s %.2f\n", "El area del Triangulo es de: ", areaTriangulo );
   
  }//Fin Método Ejercicio1
   
   public void Ejercicio2(){
     
     //Entradas
    
    Scanner entrada = new Scanner(System.in);
    int numeroUno;
    int numeroDos;
    int resultadoSuma;
  
    //Proceso
    
    System.out.println("2. Código java para introducir dos números por teclado y sumarlos.");
    System.out.println(" ");
    System.out.println("Ingrese el primer número");
    numeroUno = entrada.nextInt();
    System.out.println("Ingrese el Segundo número");
    numeroDos = entrada.nextInt();
    
    resultadoSuma = numeroUno + numeroDos;
    
    //Salida
    System.out.printf("%s %d\n", "El resultado de la suma es: ", resultadoSuma);
     
   }//Fin del método Ejecicio2
   
   public void Ejercicio3(){
     
     //Entradas
   Scanner entrada = new Scanner(System.in);
   int numeroEle;
   int resultadoOpe; 
   
   //Proceso
  System.out.println("3. Código java para introducir un número por teclado y visualizar el número elevado al cuadrado.");
  System.out.println(" ");
  System.out.println("Ingrese el Número");
  numeroEle = entrada.nextInt();
  
  resultadoOpe = numeroEle * numeroEle; 
   
   //Salidas
   System.out.printf("%s %d\n", "El número elevado es: ",resultadoOpe);
     
   }//Fin del método Ejercicio3
   
   public void Ejercicio4(){
     
     //Entradas
    Scanner entrada = new Scanner(System.in);
    int numeroUno = 1234;
    int numeroDos = 532;
    int resultadoSuma;
    
    //Proceso
    resultadoSuma = numeroUno + numeroDos;
    
    //Salidas
    System.out.println("4. Escribir un programa en Java que imprima por pantalla la suma de 1234 y 532.");    System.out.println(" ");
    System.out.printf("%s %d\n", "La Suma es de: ",resultadoSuma);
     
   }//Fin del método Ejercicio4
   
   public void Ejercicio5(){
     
    //Entradas
    Scanner entrada = new Scanner(System.in);
    int numeroUno = 1234;
    int numeroDos = 532;
    int resultadoResta;
    
    //Proceso
    resultadoResta = numeroUno - numeroDos;
    
    //Salidas
   System.out.println("5. Escribir un programa en Java que imprima por pantalla la resta de 1234 y 532.");    System.out.println(" ");
    System.out.printf("%s %d\n", "La resta es de: ",resultadoResta);
     
   }//Fin del método Ejercicio5
   
   public void Ejercicio6(){
     
      //Entradas
    Scanner entrada = new Scanner(System.in);
    int numeroUno = 1234;
    int numeroDos = 532;
    int resultadoMultiplicacion;
    
    //Proceso
    resultadoMultiplicacion = numeroUno * numeroDos;
    
    //Salidas
    System.out.println("6. Escribir un programa en Java que imprima por pantalla la multiplicación de 1234 y 532");
     System.out.println(" ");
    System.out.printf("%s %d\n", "La Multiplicacion es de: ",resultadoMultiplicacion);
     
   }//Fin del método Ejercicio6
   
   public void Ejercicio7(){
     
    //Entradas
    Scanner entrada = new Scanner(System.in);
    int numeroUno = 1234;
    int numeroDos = 532;
    int resultadoDivi;
    
    //Proceso
    resultadoDivi = numeroUno / numeroDos;
    
    //Salidas
    System.out.println("7. Escribir un programa en Java que imprima por pantalla la división de 1234 entre 532");
    System.out.println(" ");
    System.out.printf("%s %d\n", "La Division es de: ",resultadoDivi);
     
   }//Fin del método Ejercicio7
   
   public void Ejercicio8(){
     
    //Entradas
    Scanner entrada = new Scanner(System.in);
    int numeroUno = 1234;
    int numeroDos = 532;
    int resultadoModulo;
    
    //Proceso
    resultadoModulo = numeroUno % numeroDos;
    
    //Salidas
    System.out.println("8. Escribir un programa en Java que imprima por pantalla el módulo de 1234 entre 532");
    System.out.println(" ");
    System.out.printf("%s %d\n", "El resultado del Módulo es de: ",resultadoModulo);
     
   }//Fin del método Ejercicio8
   
   public void Ejercicio9(){
     
    //Entradas
    Scanner entrada = new Scanner(System.in);
    double valorEuro;
    double valorDolar = 3.000;
    double conversion;
    
    //Proceso
    System.out.println("9. Escribir un programa en Java que convierta de euros a dólares. ");
    System.out.println("Recibirá un número decimal correspondiente a la cantidad en euros ");
     System.out.println("y contestará con la cantidad correspondiente en dólares.");
    System.out.println(" ");
    System.out.println("Ingrese el valor en Euros que quiere convertir");
    valorEuro = entrada.nextDouble();
    
    conversion = valorEuro * 1.57928; 
    //Salidas
    
    System.out.printf("%f \n", conversion);
     
   }//Fin del método Ejercicio9
   
   public void Ejercicio10(){
     
     //Entradas
      Scanner entrada = new Scanner(System.in);
      double altura;
      double base;
      double area;

      //Proceso
      
      System.out.println("10. Escribir un programa en Java que calcule el área de un rectángulo del cual se le\n" +
" proporcionará por el teclado su altura y anchura (números decimales).");
      System.out.println(" ");
      System.out.println("Ingrese el valor de la altura");
      altura = entrada.nextDouble();

      System.out.println("Ingrese el valor del ancho");
      base = entrada.nextDouble();

      area = (altura*base)/2;

      //Salidas

      System.out.printf("%s %.2f\n", "El area del rectangulo es:", area);
     
   }//Fin del método Ejercicio10
   
   public void Ejercicio11(){
     
    //Entradas

    Scanner entrada = new Scanner(System.in);
    int lado;
    int area;
    int perimetro;

    //Proceso 1
    System.out.println("11. Escribir un algoritmo que pida el lado de un cuadrado ");
    System.out.println("    y muestre el valor del área y del perímetro.");
    System.out.println(" ");
    System.out.println("Ingrese el valor del lado");
    lado = entrada.nextInt();

    area = lado*lado;
    perimetro = lado*4;

    // proceso 2



    //Salidas

    System.out.printf("%s %d \n" ,"El area del cuadraro es:",area);
    System.out.printf("%s %d \n" ,"El perimetro del cuadraro es:",perimetro);
     
   }//Fin del método Ejercicio11
   
   public void Ejercicio12(){
     
    //Entradas
    Scanner entrada = new Scanner(System.in);
    double radio;
    double altura;
    double area;

    //Proceso
    System.out.println("12. Diseñar un algoritmo que determine el área y el volumen de un cilindro.");
    System.out.println("");
    System.out.println("Ingrese el radio del cilindro");
    radio = entrada.nextDouble();

    System.out.println("Ingrese altura del clindro");
    altura = entrada.nextDouble();

    area = 2 * Math.PI * radio * (altura + radio);


    //Salidas

    System.out.printf("%s %.2f\n","El area del cilindro es",area);
     
   }//Fin del método Ejercicio12
   
   public void Ejercicio13(){
     
    //Entradas
    Scanner entrada = new Scanner(System.in);
    double radio;
    int area;
    int longitud;
    double pis=3.14;
    
    //Proceso
    System.out.println("13. Realizar un algoritmo que lea el radio de una circunferencia ");
   System.out.println("     y escriba la longitud de la misma, y el área (Pi*R) ^2 del círculo inscrito.");
    System.out.println(" ");
    System.out.println("Ingrese el valor del radio");
    radio = entrada.nextInt();
    
    longitud = (int)Math.pow(radio,2);
    area = (int)Math.pow((pis*radio),2);

    //Salidas
    
    System.out.printf("El area de la circunferencia es de: %d\n",area);
    System.out.printf("La Longitud de la circunferencia es de: %d\n",longitud);
     
   }//Fin del método Ejercicio13
   
   public void Ejercicio14(){
     
    //Entradas
    Scanner entrada = new Scanner(System.in);
    int numeroUno;
    int numeroDos;
    int numeroTres;
    int promedio;
    
    //Proceso
    System.out.println("14. Calcular el promedio de tres números introducidos por teclado.");
    System.out.println("");
    System.out.println("Ingrese el Primer Numero");
    numeroUno = entrada.nextInt();
    System.out.println("Ingrese el Segundo Numero");
    numeroDos = entrada.nextInt();
    System.out.println("Ingrese el Tercer Numero");
    numeroTres = entrada.nextInt();
   
    promedio = (numeroUno + numeroDos + numeroTres)/3;
    
    //Salidas
    
    System.out.printf("El promedio de los números Ingresados es de: %d\n",promedio);
     
   }//Fin del método Ejercicio14
   
   public void Ejercicio15(){
     
    //Entradas
    Scanner entrada = new Scanner(System.in);
    
    int numeradorUno;
    int numeradorDos;
    int denominadorUno;
    int denominadorDos;

    int multiplicacionUno;
    int multiplicacionDos;
    int multiplicacionTres;
    
    int resultadoNumerador;
   
    //Proceso
    System.out.println("15. Pidiendo el ingreso del numerador y denominador ");
    System.out.println("    de 2 fracciones mostrar la suma.");
    System.out.println("");
    System.out.println("Ingrese el valor del primer numerador:");
    numeradorUno = entrada.nextInt();
    System.out.println("Ingrese el valor del primer denominador:");
    denominadorUno = entrada.nextInt();

    System.out.println("Ingrese el valor del segundo numerador");
    numeradorDos = entrada.nextInt();
    System.out.println("Ingrese el valor del segundo denominador");
    denominadorDos = entrada.nextInt();

    multiplicacionUno = numeradorUno * denominadorDos;
    multiplicacionDos = denominadorUno * numeradorDos;
    multiplicacionTres = denominadorUno * denominadorDos;
    
    resultadoNumerador = multiplicacionUno + multiplicacionDos;
    
    //Salidas
    
    System.out.printf("%d%s%d %s %d%s%d %s %d%s%d\n",numeradorUno,"/",denominadorUno,"+",numeradorDos,"/",denominadorDos,"=",resultadoNumerador,"/",multiplicacionTres);
    System.out.println("");
     
   }//Fin del método Ejercicio15
   
   public void Ejercicio16(){
     
    //Entradas
    Scanner entrada = new Scanner(System.in);
    int base;
    int exponente;
    int resultado;
    
    //Proceso
    
    System.out.println("16. Realizar un algoritmo que calcule la potencia de ");
    System.out.println("    un número real elevado a un número natural");
    System.out.println(" ");
    System.out.println("Ingrese el valor de la Base");
    base = entrada.nextInt();
    System.out.println("Ingrese el valor del Exponente");
    exponente = entrada.nextInt();
    
    resultado = (int)Math.pow(base,exponente);

    //Salidas
    
    System.out.printf("El resultado de la operación es: %d\n",resultado);
     
   }//Fin del método Ejercicio16
   
   public void Ejercicio17(){
     
     //Entradas
    Scanner entrada = new Scanner(System.in);
    int numero;

    //Proceso
    
    System.out.println("17. Código para saber si un número es positivo o negativo.");
    System.out.println(" ");
    System.out.println("Ingrese un número para evaluarlo");
    numero = entrada.nextInt();
    
     //Salidas
    
    if (numero < 0) {
      
      System.out.println("El número que ingresaste es negativo");
      
    }//Fin del if
    if(numero >= 0){
      
      System.out.println("El número que ingresaste es positivo");
    
    }//Fin del if
     
   }//Fin del método Ejercicio17
   
   public void Ejercicio18(){
     
    //Entradas
    
    Scanner entrada = new Scanner(System.in);
    int numeroUno;
    int numeroDos;

    //Proceso
    System.out.println("18. Escribir un programa en Java que lea dos números ");
    System.out.println("    del teclado y diga cuál es el mayor y cual el menor.");
    System.out.println(" ");
    System.out.println("Ingrese el Primer Número!");
    numeroUno = entrada.nextInt();
    System.out.println("Ingrese el Segundo Número");
    numeroDos = entrada.nextInt();
   
    //Salidas
    
    if(numeroUno < numeroDos){
      
      System.out.printf("El número %d%s\n",numeroDos," es MAYOR!");
      
    }//fin del if
    
    if(numeroUno > numeroDos){
      
      System.out.printf("El número %d%s\n",numeroUno," es MAYOR!");
      
    }//Fin del if
    
    if(numeroUno == numeroDos){
      
      System.out.println("Ambos... Números! SON iguales");
      
    }//Fin del if
     
   }//Fin del método Ejercicio18
   
   public void Ejercicio19(){
     
    //Entradas
    Scanner entrada = new Scanner(System.in);
    int numeroUno;
    int numeroDos;
    int numeroTres;

    //Proceso
    System.out.println("19. Escriba un programa que lea tres números enteros ");
    System.out.println("    positivos, y que calcule e imprima en pantalla el menor ");
    System.out.println("    y el mayor de todos ellos.");
    System.out.println(" ");
    System.out.println("Ingrese el valor del del primer número");
    numeroUno = entrada.nextInt();
    System.out.println("Ingrese el valor del Segundo número");
    numeroDos = entrada.nextInt();
    System.out.println("Ingrese el valor del Tercer número");
    numeroTres = entrada.nextInt();

    //Salidas
    if (numeroUno > numeroDos && numeroUno > numeroTres){
      
      System.out.printf("El numero %d%s\n", numeroUno," es MAYOR!");
      
    }//Fin del if
    
    if(numeroDos > numeroUno && numeroDos > numeroTres){
      
      System.out.printf("El numero %d%s\n", numeroDos," es MAYOR!");
      
    }//fin del if
    
    if(numeroTres > numeroUno && numeroTres > numeroDos){
      
      System.out.printf("El numero %d%s\n", numeroTres," es MAYOR!");
      
    }//Fin del if
    
    //------------------------------------------------------------------
    
     if (numeroUno < numeroDos && numeroUno < numeroTres){
      
      System.out.printf("El numero %d%s\n", numeroUno," es Menor!");
      
    }//Fin del if
    
    if(numeroDos < numeroUno && numeroDos < numeroTres){
      
      System.out.printf("El numero %d%s\n", numeroDos," es Menor!");
      
    }//fin del if
    
    if(numeroTres < numeroUno && numeroTres < numeroDos){
      
      System.out.printf("El numero %d%s\n", numeroTres," es Menor!");
      
    }//Fin del if
     
   }//Fin del método Ejercicio19
   
   public void Ejercicio20(){
     
    //Entradas
    
    Scanner entrada = new Scanner(System.in);
    String nombre="";
    int horas;
    int extras;
    int calculoHoras;

    //Proceso
    System.out.println("20. Calcular el sueldo de los empleados de una empresa. ");
    System.out.println("    Para ello se deberá pedir el nombre del empleado, las horas ");
    System.out.println("    normales trabajadas y las horas extras. Tener en cuenta que ");
    System.out.println("    el valor de la hora es de $4 y que las horas extras se pagan doble.");
    System.out.println(" ");
    System.out.println("Ingrese su nombre por Favor!");
    nombre = entrada.nextLine();
    System.out.println("Ingrese el número de horas trabajadas");
    horas = entrada.nextInt();
    System.out.println("Ingrese el número de horas Extras");
    extras = entrada.nextInt();
    
    calculoHoras = horas * 4;

    //Salidas
    if(extras > 0){
      
      int calculoExtras = (int)Math.pow(extras,2);
      int total = calculoHoras + calculoExtras;
      
      System.out.printf("Hola %s",nombre);
      System.out.printf("Sus horas trabajadas son de: %d\n", calculoHoras);
      System.out.printf("Sus horas Extras son de: %d\n", calculoExtras);
      System.out.printf("Total: %d\n",total);
      
    }//Fin if
    
    else{
      
      System.out.printf("Hola %s ",nombre);
      System.out.printf("Sus horas trabajadas suman un TOTAL de: %d\n", calculoHoras);
      
    }//Fin else
     
   }//Fin del método Ejercicio20
   
   public void Ejercicio21(){
     
    //Entradas
    
    Scanner entrada = new Scanner(System.in);
    int numeroUno;
    int numeroDos;
    

    //Proceso
    System.out.println("21. Dados dos números A y B sumarlos si A es menor a B sino restarlos.");
    System.out.println(" ");
    System.out.println("Ingrese el primer número");
    numeroUno = entrada.nextInt();
    System.out.println("Ingrese el segundo número");
    numeroDos = entrada.nextInt();

    //Salidas
    
    if(numeroUno < numeroDos){
      
      int resultado = numeroUno - numeroDos;
      System.out.printf("%d %s %d %s %d\n",numeroUno,"-",numeroDos,"=",resultado);
      
    }//Fin del if
    else{
      
      int resultado = numeroUno + numeroDos;
      System.out.printf("%d %s %d %s %d\n",numeroUno,"+",numeroDos,"=",resultado);
      
    }//Fin else
     
   }//Fin del método Ejercicio21
   
   public void Ejercicio22(){
     
     //Entradas
    
    Scanner entrada = new Scanner(System.in);
    int numeroUno;
    int numeroDos;
    int resultado;

    //Proceso
    System.out.println("22. Dados dos números A y B encontrar el cociente entre ");
    System.out.println("    A y B. Recordar que la división por cero no está definida, ");
    System.out.println("    en ese caso debe aparecer una leyenda anunciando que la ");
    System.out.println("    división n es posible.");
    System.out.println(" ");
    System.out.println("Realizaremos una Division");
    System.out.println("Ingrese el valor del número uno...");
    numeroUno = entrada.nextInt();
    System.out.println("Ingrese el valor del número dos...");
    numeroDos = entrada.nextInt();

    //Salidas
    
    if(numeroDos == 0){
      
      System.out.println("*************************************************");
      System.out.println("* Error! la División por CERO NO! está definida *");
      System.out.println("*************************************************");

    }//Fin del if
    
    if(numeroDos != 0){
      
      System.out.println("*****************************************");
      System.out.println("* BIEN HECHO! la División es POSIBLE... *");
      System.out.println("*****************************************");
      
    }//Fin del if
     
   }//Fin del método Ejercicio22
   
   public void Ejercicio23(){
     
     //Entradas
    
    Scanner entrada = new Scanner(System.in);
    byte numDia;

    //Proceso
    System.out.println("23. numDia es una variable numérica que puede tomar 7 ");
    System.out.println("    valores, ellos son 1, 2, 3, 4, 5, 6 o 7. Mostar el ");
    System.out.println("    nombre el nombre del día de la semana que corresponde ");
    System.out.println("    al valor de la variable numDia.");
    System.out.println(" ");
    System.out.println("Días de la Semana");
    System.out.println("Ingrese un número de 1-7");
    numDia = entrada.nextByte();

    //Salidas
    
    if(numDia == 1){
      
      System.out.println("Lunes");
      
    }//Fin del primer if
    
      if(numDia == 2){
        
        System.out.println("Martes");
        
      }//Fin del segundo if
        
        if(numDia == 3){
          
          System.out.println("Miercoles");
          
        }//Fin del tercer if
        
          if(numDia == 4){
            
            System.out.println("Jueves");
            
          }//Fin del cuarto if
            
            if(numDia == 5){
              
              System.out.println("Viernes");
              
            }//Fin del quinto if
            
              if(numDia == 6){
                
                System.out.println("Sabado");
                
              }//Fin del sexto if
              
                if(numDia == 7){
                  
                  System.out.println("Domingo");
                  
                }//Fin del septimo if
                
                if(numDia <=0 || numDia >= 8){
                  
                  System.out.println("Numero Invalido");
                  
                }//Fin del else
     
   }//Fin del método Ejercicio23
   
   public void Ejercicio24(){
     
    //Entradas
    Scanner entrada = new Scanner(System.in);
    
    //Los valores se declaran en orden en que se van a pedir... Sentido de las
    //manecillas del Reloj! 
    
    double longitudUno;
    double longitudDos;
    double base;

    //Proceso
    System.out.println("24. Dadas las longitudes de los tres lados de un triángulo ");
    System.out.println("    determinar si es equilátero o no.");
    System.out.println(" ");
    System.out.println("Ingrese el valor de la longitud Uno...");
    longitudUno = entrada.nextDouble();
    System.out.println("Ingrese el valor de la longitud Dos...");
    longitudDos = entrada.nextDouble();
    System.out.println("Ingrese el valor de la base");
    base = entrada.nextDouble();

    //Salidas
    if (longitudUno == longitudDos && longitudUno == base){
      
      System.out.println("Es un triangulo equilátero :)");
      
    }//Fin del if
    
    if (longitudUno != longitudDos && longitudUno != base){
      
      System.out.println("NO! es un triangulo equilátero :(");
      
    }//Fin del if
     
   }//Fin del método Ejercicio24
   
   public void Ejercicio25(){
     
    //Entradas
    
    Scanner entrada = new Scanner(System.in);
    Double numeroUno;
    Double numeroDos;
   
    //Proceso
    System.out.println("25. Dados dos números A y B sumarlos si al menos uno de ");
    System.out.println("    ellos es negativo en caso contrario multiplicarlos.");
     System.out.println(" ");
    System.out.println("Ingrese el primer número");
    numeroUno = entrada.nextDouble();
    System.out.println("Ingrese el segundo número");
    numeroDos = entrada.nextDouble();

    //Salidas
    
    if(numeroUno < 0 || numeroDos < 0){
      
      Double resultado = numeroUno + numeroDos;
      System.out.printf("%.1f %s %.1f %s %.1f\n",numeroUno,"+",numeroDos,"=",resultado);
      
    }//Fin del if
    else{
      
      Double resultado = numeroUno * numeroDos;
      System.out.printf("%.1f %s %.1f %s %.1f\n",numeroUno,"*",numeroDos,"=",resultado);
      
    }//Fin del else
     
   }//Fin del método Ejercicio25
   
   public void Ejercicio26(){
     
     //Entradas
    
    Scanner entrada = new Scanner(System.in);
    int dia;
    int mes;
    
    //Proceso
    System.out.println("26. Pidiendo el día y el mes de nacimiento mostrar el signo.");
    System.out.println(" ");
    System.out.println("Ingrese el Día");
    dia = entrada.nextInt();
    
    System.out.println("Ingrese el mes");

//    System.out.println("Enero = 1");
//    System.out.println("Febrero = 2");
//    System.out.println("Marzo = 3");
//    System.out.println("Abril = 4");
//    System.out.println("Mayo = 5");
//    System.out.println("Junio = 6");
//    System.out.println("Julio = 7");
//    System.out.println("Agosto = 8");
//    System.out.println("Septiembre = 9");
//    System.out.println("Octubre = 10");
//    System.out.println("Noviembre = 11");
//    System.out.println("Diciembre = 12");
//    
    mes = entrada.nextInt();
    
    //Salidas
    
    //-------------------------------------------------
    
    if(dia >=21 && dia <=30 && mes == 3 || dia >= 1 && dia <= 19 && mes == 4){
      
      System.out.println("Zigno Aries ");
      
    }//Fin del if de Aries
    
    //-------------------------------------------------
    
    if(dia >=20 && dia <=30 && mes == 4 || dia >= 1 && dia <= 20 && mes == 5){
      
      System.out.println("Zigno Tauro ");
      
    }//Fin del if de Tauro
    
    //-------------------------------------------------
    
    if(dia >=21 && dia <=30 && mes == 3 || dia >= 1 && dia <= 19 && mes == 4){
      
      System.out.println("Zigno Aries ");
      
    }//Fin del if Aries
    
    //-------------------------------------------------
    
    if(dia >=21 && dia <=30 && mes == 5 || dia >= 1 && dia <= 20 && mes == 6){
      
      System.out.println("Zigno Géminis ");
      
    }//Fin del if de Géminis
    
    //-------------------------------------------------
    
    if(dia >=21 && dia <=30 && mes == 6 || dia >= 1 && dia <= 22 && mes == 7){
      
      System.out.println("Zigno Cáncer ");
      
    }//Fin del if de Cáncer
    
    //-------------------------------------------------
    
    if(dia >=23 && dia <=30 && mes == 7 || dia >= 1 && dia <= 23 && mes == 8){
      
      System.out.println("Zigno Leo ");
      
    }//Fin del if de Leo
    
    //-------------------------------------------------
    
     if(dia >=24 && dia <=30 && mes == 8 || dia >= 1 && dia <= 22 && mes == 9){
      
      System.out.println("Zigno Virgo ");
      
    }//Fin del if de Virgo
     
    //-------------------------------------------------
     
     if(dia >=23 && dia <=30 && mes == 9 || dia >= 1 && dia <= 22 && mes == 10){
      
      System.out.println("Zigno Libra ");
      
    }//Fin del if de Libra
     
    //-------------------------------------------------
     
     if(dia >=23 && dia <=30 && mes == 10 || dia >= 1 && dia <= 21 && mes == 11){
      
      System.out.println("Zigno Escorpio ");
      
    }//Fin del if de Escorpio
     
    //-------------------------------------------------
    
     if(dia >=22 && dia <=30 && mes == 11 || dia >= 1 && dia <= 21 && mes == 12){
      
      System.out.println("Zigno Sagitario ");
      
    }//Fin del if de Sagitario
     
    //-------------------------------------------------
    
    if(dia >=22 && dia <=30 && mes == 12 || dia >= 1 && dia <= 19 && mes == 1){
      
      System.out.println("EZigno Capricornio ");
      
    }//Fin del if de Capricornio
     
    //-------------------------------------------------
    
    if(dia >=20 && dia <=30 && mes == 1 || dia >= 1 && dia <= 19 && mes == 2){
      
      System.out.println("Zigno Acuario ");
      
    }//Fin del if de Acuario
     
    //-------------------------------------------------
    
    if(dia >=20 && dia <=30 && mes == 2 || dia >= 1 && dia <= 20 && mes == 3){
      
      System.out.println("Zigno Piscis ");
      
    }//Fin del if de Piscis
     
    //-------------------------------------------------
     
     
   }//Fin del método Ejercicio26
   
   public void Ejercicio27(){
     
    //Entradas
    
    Scanner entrada = new Scanner(System.in);
    double base;
    double altura;
   
    //Proceso
    System.out.println("27. Pidiendo el ingreso de la base y la altura de un ");
    System.out.println("    cuadrilátero, indicar si es cuadrado o rectángulo. ");
    System.out.println("    Para cualquiera de los dos casos mostrar el perímetro ");
    System.out.println("    y la superficie de la figura.");
    System.out.println(" ");
    System.out.println("Ingrese el valor de la Base por favor!");
    base = entrada.nextDouble();
    System.out.println("Ingrese el valor de la Altura por favor!");
    altura = entrada.nextDouble();

    //Salidas
    
    if (base == altura) {
      
      //Cuadrado
      
      int perimetro = (int) Math.pow(base,4);
      int superficie = (int) Math.pow(base,2);
      
      System.out.println("Es un cuadrado");     
      System.out.printf("El perímetro es de: %d\n",perimetro);
      System.out.printf("La superficie es de: %d\n",superficie);
    
    }//Fin del if
    
    if (base != altura) {
      
      //Rectangulo
      
      int perimetro = (int)(base + base + altura + altura);
      int superficie = (int) (base * altura);
      
      System.out.println("Es un Rectangulo");     
      System.out.printf("El perímetro es de: %d\n",perimetro);
      System.out.printf("La superficie es de: %d\n",superficie);
    
    }//Fin del if
     
   }//Fin del método Ejercicio27
   
   public void Ejercicio28(){
     
     //Entradass
    Scanner entrada = new Scanner(System.in);
    double compra;

    //Proceso
    System.out.println("28. Un negocio hace descuentos en las ventas de sus ");
    System.out.println("    productos. Si la compra es inferior a $100 el descuento ");
    System.out.println("    es del 5%, si es mayor o igual a 100 y menor a 200 el ");
    System.out.println("    descuento es del 10% sino será del 15%. Dado el precio ");
    System.out.println("    de la venta mostrar el descuento realizado en pesos y el ");
    System.out.println("    precio final con descuento.");
    System.out.println("");
    System.out.println("Ingrese el valor de la compra");
    compra = entrada.nextDouble();

    //Salidas
    if (compra < 100) {

      double descuento = (compra * 5) / 100;
      double totalCompra = (double) compra - descuento;
      int descuentoPesos = (int) descuento * 3000;
      int totalCompraPesos = (int) totalCompra * 3000;

      System.out.printf("El valor de la compra fue de: $%.2f\n", compra);
      System.out.printf("Descuento en Pesos: %d%s\n", descuentoPesos, " pesos");
      System.out.printf("Precio Final con Descuento: %d%s\n", totalCompraPesos, " pesos");

    }//Fin del if

    if (compra >= 100 && compra < 200) {

      double descuento = (compra * 10) / 100;
      double totalCompra = (double) compra - descuento;
      int descuentoPesos = (int) descuento * 3000;
      int totalCompraPesos = (int) totalCompra * 3000;

      System.out.printf("El valor de la compra fue de: $%.2f\n", compra);
      System.out.printf("Descuento en Pesos: %d%s\n", descuentoPesos, " pesos");
      System.out.printf("Precio Final con Descuento: %d%s\n", totalCompraPesos, " pesos");

    }//Fin del if
    else {

      double descuento = (compra * 15) / 100;
      double totalCompra = (double) compra - descuento;
      int descuentoPesos = (int) descuento * 3000;
      int totalCompraPesos = (int) totalCompra * 3000;

      System.out.printf("El valor de la compra fue de: $%.2f\n", compra);
      System.out.printf("Descuento en Pesos: %d%s\n", descuentoPesos, " pesos");
      System.out.printf("Precio Final con Descuento: %d%s\n", totalCompraPesos, " pesos");

    }//Fin del else
     
   }//Fin del método Ejercicio28
   
   public void Ejercicio29(){
     
    //Entradas
    Scanner entrada = new Scanner(System.in);

    int ingresoDeNacimientos;
    int dia;
    int mes;
    int año;
    byte sexo;
    int contadorFemenino;
    int contadorMasculino;

    contadorFemenino = 0;
    contadorMasculino = 0;

    //-------------------------------------------------------------------
    //Proceso
    System.out.println("29. Pedir el ingreso de los datos de nacimientos de un ");
    System.out.println("    hospital: día, mes, año y sexo (1-femenino 2-masculino). ");
    System.out.println("    Muestra el total de varones, el total de mujeres, el total general.");
    System.out.println(" ");
    System.out.println("Ingrese el número de nacimientos del Hospital");
    ingresoDeNacimientos = entrada.nextInt();

    for (int i = 1; i <= ingresoDeNacimientos; i++) {

      System.out.println("Ingrese el día de nacimiento");
      dia = entrada.nextInt();
      System.out.println("Ingrese el mes de nacimiento");
      mes = entrada.nextInt();
      System.out.println("Ingrese el año de nacimiento");
      año = entrada.nextInt();

      System.out.println("Ingrese el sexo (1-femenino 2-masculino)");
      sexo = entrada.nextByte();

      if (sexo == 1) {

        contadorFemenino++;
      }

      if (sexo == 2) {

        contadorMasculino++;

      }//fin if 

      System.out.println("-------------------------");

    }//Fin del for

    //Salidas
    int totalGeneral = contadorFemenino + contadorMasculino;
    System.out.printf("El total de Varones es de: %d\n", contadorMasculino);
    System.out.printf("El total de Mujeres es de: %d\n", contadorFemenino);
    System.out.printf("El total de Nacimientos es de: %d\n ", totalGeneral);
     
   }//Fin del método Ejercicio29
   
   public void Ejercicio30(){
     
     //Entradas
    Scanner entrada = new Scanner(System.in);
    int cantidadPersonas;
    int sexo;
    byte estadoCivil;
    int contadorHombre;
    int contadorMujer;
    int contadorHombreSoltero;
    int contadorMujerSoltera;
    int contadorHombreCasado;
    int contadorMujerCasada;
    int contadorHombreViudo;
    int contadorMujerViuda;
    int contadorHombreDivorcio;
    int contadorMujerDivorcio;

    contadorHombre = 0;
    contadorMujer = 0;
    contadorHombreSoltero = 0;
    contadorMujerSoltera = 0;
    contadorHombreCasado = 0;
    contadorMujerCasada = 0;
    contadorHombreViudo = 0;
    contadorMujerViuda = 0;
    contadorHombreDivorcio = 0;
    contadorMujerDivorcio = 0;

    //Proceso
    System.out.println("30. Se deberá pedir el sexo (F-M) y el estado civil (S-C-V-D) ");
    System.out.println("    de las personas que lleguen a un espectáculo. Se deberán mostrar ");
    System.out.println("    la cantidad y el porcentaje de hombres solteros, mujeres solteras, ");
    System.out.println("    hombres casados, mujeres casadas, etc.");
    System.out.println(" ");
    System.out.println("Ingrese la cantidad de personas que asistiras al espectaculo");
    cantidadPersonas = entrada.nextInt();
    
    System.out.println("--------------------------");

    for (int i = 1; i <= cantidadPersonas; i++) {

      System.out.println("Ingrese su sexo (F=1)(M=2)");
      sexo = entrada.nextInt();
      
      while(sexo <= 0 || sexo >= 3){
        
        System.out.println("****************************");
        System.out.println("* ERROR! Valor... Invalido *");
        System.out.println("*   --> (F=1)(M=2) <--     *");
        System.out.println("****************************");
        System.out.println("--> Ingrese un nuevo valor");
        sexo = entrada.nextInt();
        
      }//Fin del while

      switch (sexo) {

        case 1:
          
          contadorMujer++;
          
          System.out.println("Ingrese su estado Civil en número");
          System.out.println("(1=Soltero) (2=Casado) (3=Viudo) (4=Divorcio)");
          estadoCivil = entrada.nextByte();
          
          while(estadoCivil <= 0 || estadoCivil >= 5){
        
            System.out.println("*************************************************");
            System.out.println("*          ERROR! Valor... Invalido                *");
            System.out.println("* (1=Soltero) (2=Casado) (3=Viudo) (4=Divorcio) *");
            System.out.println("***********************************************");
            System.out.println("--> Ingrese un nuevo valor");
            estadoCivil = entrada.nextByte();
        
          }//Fin del while
          
          switch(estadoCivil){
            
            case 1:
              contadorMujerSoltera++;
              break;
              
            case 2:
              contadorMujerCasada++;
              break;
              
            case 3:
              contadorMujerViuda++;
              break;
              
            case 4:
              contadorMujerDivorcio++;
             break;
             
          }//Fin del switch Estado Civil Mujer
          
          break; //break; case 1:
         
        case 2:
          
          contadorHombre++;
          
          System.out.println("Ingrese su estado Civil en número");
          System.out.println("(1=Soltero) (2=Casado) (3=Viudo) (4=Divorcio)");
          estadoCivil = entrada.nextByte();
          
          while(estadoCivil <= 0 || estadoCivil >= 5){
        
            System.out.println("*************************************************");
            System.out.println("*          ERROR! Valor... Invalido             *");
            System.out.println("* (1=Soltero) (2=Casado) (3=Viudo) (4=Divorcio) *");
            System.out.println("***********************************************");
            System.out.println("--> Ingrese un nuevo valor");
            estadoCivil = entrada.nextByte();
        
          }//Fin del while
          
          switch(estadoCivil){
            
            case 1:
              contadorHombreSoltero++;
              break;
             
            case 2:
              contadorHombreCasado++;
              break;
              
            case 3:
              contadorHombreViudo++;
              break;
              
            case 4:
              contadorHombreDivorcio++;
              break;
            
          }//Fin del switch Estado Civil Hombre
          
          break; //break case 2:
          
      }//Fin switch para el sexo
      
    }//Fin del for
    
    //Operaciones
    
    double  sumaContador = contadorMujer + contadorHombre;
    
    System.out.println("***************************");
    System.out.println("* TOTAL HOMBRES Y MUJERES *");
    System.out.println("***************************");
    System.out.printf("--> Hombres: %d\n",contadorHombre);
    System.out.printf("--> Mujeres: %d\n",contadorMujer);
    System.out.printf("-->  Total = %.0f\n",sumaContador);
    
    double  porcentajeHombres = (contadorHombre / sumaContador) * 100;
    double  porcentajeMujeres = (contadorMujer / sumaContador) * 100;
    
    System.out.printf("--> Porcentaje de Hombres : %.0f%s\n",porcentajeHombres,"%");
    System.out.printf("-- >Porcentaje de Mujeres : %.0f%s\n",porcentajeMujeres,"%");
    
    System.out.println("");
    
    double sumaContadorSoltero = contadorMujerSoltera + contadorHombreSoltero;
    double porcentajeHombreSol = (contadorHombreSoltero / sumaContadorSoltero) * 100;
    double porcentajeMujerSol = (contadorMujerSoltera / sumaContadorSoltero) * 100;
    
    System.out.println("****************************************");
    System.out.println("* TOTAL HOMBRES Y MUJERES --> SOLTEROS *");
    System.out.println("****************************************");
     System.out.printf("--> Hombres: %d\n",contadorHombreSoltero);
    System.out.printf("--> Mujeres: %d\n",contadorMujerSoltera);
    System.out.printf("-->  Total = %.0f\n",sumaContadorSoltero);
    System.out.printf("--> Porcentaje de Hombres: %.0f%s\n",porcentajeHombreSol,"%");
    System.out.printf("--> Porcentaje de Mujeres: %.0f%s\n",porcentajeMujerSol,"%");
     
    System.out.println(" ");
    
    double sumaContadorCasado = contadorMujerCasada + contadorHombreCasado;
    double porcentajeHombreCas = (contadorHombreCasado / sumaContadorCasado) * 100;
    double porcentajeMujerCas = (contadorMujerCasada / sumaContadorCasado) * 100;
    
    System.out.println("****************************************");
    System.out.println("* TOTAL HOMBRES Y MUJERES --> CASADOS  *");
    System.out.println("****************************************");
     System.out.printf("--> Hombres: %d\n",contadorHombreCasado);
    System.out.printf("--> Mujeres: %d\n",contadorMujerCasada);
    System.out.printf("-->  Total = %.0f\n",sumaContadorCasado);
    System.out.printf("--> Porcentaje de Hombres: %.0f%s\n",porcentajeHombreCas,"%");
    System.out.printf("--> Porcentaje de Mujeres: %.0f%s\n",porcentajeMujerCas,"%");
    
    System.out.println(" ");
    
    double sumaContadorViudo = contadorMujerViuda + contadorHombreViudo;
    double porcentajeHombreViu = (contadorHombreViudo / sumaContadorViudo) * 100;
    double porcentajeMujerViu = (contadorMujerCasada / sumaContadorViudo) * 100;
    
    System.out.println("****************************************");
    System.out.println("* TOTAL HOMBRES Y MUJERES --> VIUDO    *");
    System.out.println("****************************************");
     System.out.printf("--> Hombres: %d\n",contadorHombreViudo);
    System.out.printf("--> Mujeres: %d\n",contadorMujerViuda);
    System.out.printf("-->  Total = %.0f\n",sumaContadorViudo);
    System.out.printf("--> Porcentaje de Hombres: %.0f%s\n",porcentajeHombreViu,"%");
    System.out.printf("--> Porcentaje de Mujeres: %.0f%s\n",porcentajeMujerViu,"%");
    
    System.out.println(" ");
    
    double sumaContadorDivorcio = contadorMujerDivorcio + contadorHombreDivorcio;
    double porcentajeHombreDiv = (contadorHombreDivorcio / sumaContadorDivorcio) * 100;
    double porcentajeMujerDiv = (contadorMujerDivorcio / sumaContadorDivorcio) * 100;
    
    System.out.println("****************************************");
    System.out.println("* TOTAL HOMBRES Y MUJERES --> DIVORCIO *");
    System.out.println("****************************************");
     System.out.printf("--> Hombres: %d\n",contadorHombreDivorcio);
    System.out.printf("--> Mujeres: %d\n",contadorMujerDivorcio);
    System.out.printf("-->  Total = %.0f\n",sumaContadorDivorcio);
    System.out.printf("--> Porcentaje de Hombres: %.0f%s\n",porcentajeHombreDiv,"%");
    System.out.printf("--> Porcentaje de Mujeres: %.0f%s\n",porcentajeMujerDiv,"%");
     
   }//Fin del método Ejercicio30
   
   public void Ejercicio31(){
     
    Scanner entrada = new Scanner(System.in);
    int anio;
    
    System.out.println("31. Realizar un algoritmo que determine si un año es ");
    System.out.println("    bisiesto o no lo es.");
    System.out.println(" ");
    System.out.println("Hola! por favor ingrese el año para saber si es Bisiesto");
    anio = entrada.nextInt();
    
   if ((anio % 4 == 0) && ((anio % 100 != 0) || (anio % 400 == 0))){
     
    System.out.println("El año es bisiesto");
    
   }//Fin del if
   else{
    
    System.out.println("El año no es bisiesto"); 
  
  }//Fin del else
     
   }//Fin del método Ejercicio31
   
   public void Ejercicio32(){
     
    Scanner entrada = new Scanner(System.in);
    int dia;
    int diaDos;
    int mes;
    int mesDos;
    int anio;
    int contador = 0;
    int contadorDiaUno = 0;
    int contadorDiaDos = 0;
    int contadorMes = 0;
    int sumaMes = 0;
    int sumaContadorDia;
    
    
    System.out.println("32. Dados el día y mes de dos fechas, donde la primera ");
    System.out.println("    fecha es menor a la segunda y ambas pertenece a al mismo ");
    System.out.println("    año, calcular los días que median entre ambas. Suponiendo ");
    System.out.println("    que todos los meses tienen treinta días.");
    System.out.println(" ");
    System.out.println("Primera --> Fecha:");
    System.out.println("--> Ingrese el día...");
    dia = entrada.nextInt();
   
    int entrar = 0;
    
    while(entrar == 0){
      
      while(dia <= 0 || dia > 30){
        
        System.out.println("*************************");
        System.out.println("* Error! Día Incorrecto *");
        System.out.println("*************************");
        dia = entrada.nextInt();
        
      }//Fin del while
      
      if(dia > 0 && dia <= 30){
       
      entrar = 1;
      
      System.out.println("Bien! HECHO... Gracias");
      System.out.println(" ");
      
      }//Fin del If
      
    }//Fin del while validador de Día
    
    //---------------------
    
    System.out.println("--> Ingrese el mes...");
    mes = entrada.nextInt();
    
    entrar = 0;
   
    while( entrar == 0){
      
      if (mes <= 0 || mes > 12) {
        
        System.out.println("*************************");
        System.out.println("* Error! Mes Incorrecto *");
        System.out.println("*************************");
        mes = entrada.nextInt();
        
      }//Fin del If
      
      if(mes > 0 && mes <= 12){
        
      entrar = 1;
      
      System.out.println("Bien! HECHO... Gracias");
      System.out.println(" ");
      
      }//Fin del if
      
    }//Fin del While validador del mes
    
    
    //---------------------
    
    System.out.println("--> Ingrese el año para las DOS FECHAS!");
    anio = entrada.nextInt();
    
    entrar = 0;
    
    while(entrar == 0){
      
      int anioOri = anio;
      
      while(anio != 0){
        
        anio = anio / 10;
        contador++;
        
      }//Fin del While validador
      
      if(contador > 3 && contador < 5) {
        
        entrar = 1;
        System.out.println("Muy! Bien Gracias...");
        
      }//Fin del if contador
      
      if (contador < 4 || contador >= 5) {
        
        entrar = 0;
        System.out.println("**************************");
        System.out.println("* ERROR! Año INVALIDO... *");
        System.out.println("**************************");
        System.out.println("--> Ingrese el año");
        anio = entrada.nextInt();
        
        contador = 0;
        
      }//Fin del if
       
    }//Fin del while
    
    System.out.println("----------------------------");
    
    System.out.println(" ");
    System.out.println("SEGUNDA --> Fecha:");
    System.out.println("--> Ingrese el día...");
    diaDos = entrada.nextInt();
    
    entrar = 0;
    
    while(entrar == 0){
      
      while(diaDos <= 0 || diaDos > 30){
        
        System.out.println("*************************");
        System.out.println("* Error! Día Incorrecto *");
        System.out.println("*************************");
        diaDos = entrada.nextInt();
        
      }//Fin del while
      
      if(diaDos > 0 && diaDos <= 30){
       
      entrar = 1;
      
      System.out.println("Bien!");
      System.out.println(" ");
      
      }//Fin del If
      
    }//Fin del while validador de DíaDos
    
      //-----------------------------
      
    System.out.println("--> Ingrese el mes...");
    mesDos = entrada.nextInt();
    
    entrar = 0;
   
    while( entrar == 0){
      
      if (mesDos <= 0 || mesDos > 12) {
        
        System.out.println("*************************");
        System.out.println("* Error! Mes Incorrecto *");
        System.out.println("*************************");
        mesDos = entrada.nextInt();
        
      }//Fin del If
      
      if(mesDos > 0 && mesDos <= 12){
        
      entrar = 1;
      
      }//Fin del if
      
    }//Fin del While validador del mes
    
       while(mesDos <= mes){
        
        System.out.println("**************************************************************************");
        System.out.println("* ERROR! el MES NO! PUEDE ser MENOR o IGUAL al primer MES que Ingresaste *");
        System.out.println("**************************************************************************");
        System.out.println("--> Ingrese el Mes...");
        mesDos = entrada.nextInt();
        
      }//Fin del while
       
       //-------------------------------------
       
       //Calculo 
       
       //Contador Día Uno
       for (int i = 1; i <= dia; i++) {
         
         contadorDiaUno++;
          
       }//Fin del For
        
       //Contador de meses
       for(int j = mes;j < mesDos; j++){
         
        sumaMes = sumaMes + 1;
       
       }//Fin del if 
       
      
//       System.out.printf("Esto es sumaMes %d\n",sumaMes);
  
       if (sumaMes <= 1) {
         
         sumaContadorDia = contadorDiaUno + diaDos;
         System.out.printf("Días = %d\n",sumaContadorDia);
         
        }//Fin if
       
       if (sumaMes == 2) {
         
         sumaContadorDia = (contadorDiaUno + diaDos) + 30;
         System.out.printf("Días = %d\n",sumaContadorDia);
         
        }//Fin if
       
       if (sumaMes == 3) {
         
         sumaContadorDia = (contadorDiaUno + diaDos) + (30 * 2);
         System.out.printf("Días = %d\n",sumaContadorDia);
         
        }//Fin if
       
       if (sumaMes == 4) {
         
         sumaContadorDia = (contadorDiaUno + diaDos) + (30 * 3);
         System.out.printf("Días = %d\n",sumaContadorDia);
         
        }//Fin if
       
       if (sumaMes == 5) {
         
         sumaContadorDia = (contadorDiaUno + diaDos) + (30 * 4);
         System.out.printf("Días = %d\n",sumaContadorDia);
         
        }//Fin if
       
       if (sumaMes == 6) {
         
         sumaContadorDia = (contadorDiaUno + diaDos) + (30 * 5);
         System.out.printf("Días = %d\n",sumaContadorDia);
         
        }//Fin if
       
       if (sumaMes == 7) {
         
         sumaContadorDia = (contadorDiaUno + diaDos) + (30 * 6);
         System.out.printf("Días = %d\n",sumaContadorDia);
         
        }//Fin if
       
       if (sumaMes == 8) {
         
         sumaContadorDia = (contadorDiaUno + diaDos) + (30 * 7);
         System.out.printf("Días = %d\n",sumaContadorDia);
         
        }//Fin if
       
       if (sumaMes == 9) {
         
         sumaContadorDia = (contadorDiaUno + diaDos) + (30 * 8);
         System.out.printf("Días = %d\n",sumaContadorDia);
         
        }//Fin if
       
       if (sumaMes == 10) {
         
         sumaContadorDia = (contadorDiaUno + diaDos) + (30 * 9);
         System.out.printf("Días = %d\n",sumaContadorDia);
         
        }//Fin if
       
       if (sumaMes == 11) {
         
         sumaContadorDia = (contadorDiaUno + diaDos) + (30 * 10);
         System.out.printf("Días = %d\n",sumaContadorDia);
         
        }//Fin if
       
       if (sumaMes == 12) {
         
         sumaContadorDia = (contadorDiaUno + diaDos) + (30 * 11);
         System.out.printf("Días = %d\n",sumaContadorDia);
         
        }//Fin if
     
   }//Fin del método Ejercicio32
   
   public void Ejercicio33(){
     
      Scanner entrada = new Scanner(System.in);
      int horaEntrada;
      int minutoEntrada;
      int segundoEntrada;
      int formatoEntrada;
      
      int horaSalida;
      int minutoSalida;
      int segundoSalida;
      int formatoSalida;
      
      int horaTotal;
      int minutoTotal;
      int segundoTotal;
      
//      int ban;
//  
//      do{
//      try{
//
//      ban = 0;
      
      System.out.println("33. Pidiendo la hora de ingreso y la hora de salida ");
      System.out.println("    mostrar cuanto tiempo transcurrió. Las horas deberán ");
      System.out.println("    pedirse como HI, MI, SI (hora de ingreso, minuto de ");
      System.out.println("    ingreso y segundo de ingreso) y como HS, MS, SS ");
      System.out.println("    (hora de salida, minuto de salida y segundo de salida)");
      System.out.println(" ");
      
      System.out.println("--> Ingrese la Hora de Ingreso...");
      horaEntrada = entrada.nextInt();
      
      while(horaEntrada < 0 || horaEntrada > 12){
        
      System.out.println("xxxxxxxxxxxxxxxxxxxxxxxxxxxx");
      System.out.println("   Ups!! HORA INVALIDA!");
      System.out.println("xxxxxxxxxxxxxxxxxxxxxxxxxxxx");
      System.out.println("--> Ingrese la Hora de Entrada...");
      horaEntrada = entrada.nextInt();
        
      }//Fin del while que valida, si la hora está en el rango correcto! ------
      
      System.out.println("--> Ingrese el Minuto de Ingreso...");
      minutoEntrada = entrada.nextInt();
      
      while(minutoEntrada < 0 || minutoEntrada > 59){
        
      System.out.println("xxxxxxxxxxxxxxxxxxxxxxxxxxxx");
      System.out.println("  Ups!! HORA INVALIDA!");
      System.out.println("xxxxxxxxxxxxxxxxxxxxxxxxxxxx");
      System.out.println("--> Ingrese la Hora de Entrada...");
      minutoEntrada = entrada.nextInt();
        
      }//Fin del while que valida, si la minuto está en el rango correcto! ------
      
      System.out.println("--> Ingrese el Segundo de Entrada...");
      segundoEntrada = entrada.nextInt();
      
      while(segundoEntrada < 0 || segundoEntrada > 59){
        
      System.out.println("xxxxxxxxxxxxxxxxxxxxxxxxxxxx");
      System.out.println("  Ups!! HORA INVALIDA!");
      System.out.println("xxxxxxxxxxxxxxxxxxxxxxxxxxxx");
      System.out.println("--> Ingrese la Hora de Entrada...");
      segundoEntrada = entrada.nextInt();
        
      }//Fin del while que valida, si el segundo está en el rango correcto! ------
      
      System.out.println("--> Seleccione el Formato! de Entrada... ");
      System.out.println("--> 1-AM -- 2-PM");
      formatoEntrada = entrada.nextInt();
      
      while(formatoEntrada < 1 || formatoEntrada > 2){
        
      System.out.println("xxxxxxxxxxxxxxxxxxxxxxxxxxxx");
      System.out.println("  Ups!! FORMATO INVALIDO!");
      System.out.println("xxxxxxxxxxxxxxxxxxxxxxxxxxxx");
      System.out.println("--> Ingrese el Formato de Entrada...");
      formatoEntrada = entrada.nextInt();
        
      }//Fin del while que valida, si el formato está en el rango correcto! ------
      
      
      System.out.println("--> Ingrese la Hora de Salida...");
      horaSalida = entrada.nextInt();
      
      while(horaSalida < 0 || horaSalida > 12){
        
      System.out.println("xxxxxxxxxxxxxxxxxxxxxxxxxxxx");
      System.out.println("  Ups!! HORA INVALIDO!");
      System.out.println("xxxxxxxxxxxxxxxxxxxxxxxxxxxx");
      System.out.println("--> Ingrese la Hora de Entrada...");
      horaSalida = entrada.nextInt();
      
      }//Fin del While que valida, si la hora de Salida está en el rango correcto ----
      
      System.out.println("--> Ingrese el Minuto de Salida");
      minutoSalida = entrada.nextInt();
      
      while(minutoSalida < 0 || minutoSalida > 59){
        
      System.out.println("xxxxxxxxxxxxxxxxxxxxxxxxxxxx");
      System.out.println("  Ups!! MINUTO INVALIDO!");
      System.out.println("xxxxxxxxxxxxxxxxxxxxxxxxxxxx");
      System.out.println("--> Ingrese la Hora de Entrada...");
      minutoSalida = entrada.nextInt();
        
      }//Fin del while que valida, si el segundo está en el rango correcto! ------
      
      System.out.println("--> Ingrese el Segundo de Salida...");
      segundoSalida = entrada.nextInt();
      
      while(segundoSalida < 0 || segundoSalida > 59){
        
      System.out.println("xxxxxxxxxxxxxxxxxxxxxxxxxxxx");
      System.out.println("  Ups!! SEGUNDO INVALIDO!");
      System.out.println("xxxxxxxxxxxxxxxxxxxxxxxxxxxx");
      System.out.println("--> Ingrese la Hora de Entrada...");
      segundoSalida = entrada.nextInt();
        
      }//Fin del while que valida, si el segundo está en el rango correcto! ------
      
      System.out.println("--> Seleccione el Formato! de Salida... ");
      System.out.println("--> 1-AM -- 2-PM");
      formatoSalida = entrada.nextInt();
      
      while(formatoSalida < 1 || formatoSalida > 2){
        
      System.out.println("xxxxxxxxxxxxxxxxxxxxxxxxxxxx");
      System.out.println("  Ups!! FORMATO INVALIDO!");
      System.out.println("xxxxxxxxxxxxxxxxxxxxxxxxxxxx");
      System.out.println("--> Ingrese el Formato de Salida...");
      formatoSalida = entrada.nextInt();
        
      }//Fin del while que valida, si el formato está en el rango correcto! ------
      
      
      //Validaciones---
      
      if (formatoEntrada == 1 && formatoSalida == 1) {
        
        horaTotal = horaSalida - horaEntrada;
        minutoTotal = minutoSalida - minutoEntrada;
        segundoTotal = segundoSalida - segundoEntrada;
        
      }//Fin if formato
      
      else if (formatoEntrada == 2 && formatoSalida == 2) {
        
        horaTotal = horaSalida - horaEntrada;
        minutoTotal = minutoEntrada - minutoSalida;
        segundoTotal = segundoEntrada - segundoSalida;
        
      }//Fin del else if
      
      else{
        
        horaTotal = (horaSalida + 12) - horaEntrada;
        minutoTotal = minutoEntrada - minutoSalida;
        segundoTotal = segundoEntrada - segundoSalida;
        
      }//Fin else
      
      if (horaTotal < 0) {
        
        horaTotal = horaTotal * (-1);
        
      }//Fin if
      
      if (minutoTotal < 0) {
        
        minutoTotal = minutoTotal * (-1);
        
      }//Find el if
      
      if(segundoTotal < 0){
        
        segundoTotal = segundoTotal * (-1);
        
      }//Fin del if
      
      System.out.printf("Tiempo Transcurrido: %d%s:%d%s:%d\n",horaTotal,"",minutoTotal,"",segundoTotal);
      
      
//      }catch(Exception e){
//        
//      System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXX");  
//      System.out.println(" Ups! Caracter INVALIDO!");
//      System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXX");
//      
//      ban = 1;
//      entrada.nextLine();
//      
//    }//Fin Catch
//    }while(ban != 0);                                                            
     
   }//Fin del método Ejercicio33
   
   public void Ejercicio34(){
     
     //Entradas
    int multiploDeTres;
    System.out.println("34. Imprimir todos los múltiplos de 3 que hay entre 1 y 100.");
    System.out.println(" ");
    //Proceso
    for (int i = 1; i <= 100; i++) {

      if (i % 3 == 0) {

        System.out.printf("%d\n", i);

      }//Fin del if

    }//Fin del For
     
   }//Fin del método Ejercicio34
   
   public void Ejercicio35(){
     
     System.out.println("35. Imprimir los números impares entre 0 y 100.");
     System.out.println(" ");
     for (int i = 0; i <= 100; i++) {
      
      if (i % 2 != 0 ){
        
      System.out.printf("%d\n",i);
      
      }//Fin del if
      
    }//Fin del for
     
   }//Fin del método Ejercicio35
   
   public void Ejercicio36(){
     
     System.out.println("36. Imprimir los números pares del 0 al 100.");
     System.out.println(" ");
     for(int i = 1; i <= 100; i++){
      
      if(i % 2 == 0){
        
        System.out.printf("%d\n",i);
        
      }//Fin del if
    
    }//fin del for
     
   }//Fin del método Ejercicio36
   
   public void Ejercicio37(){
     
     System.out.println("37. Escribir un programa en Java que imprima por ");
     System.out.println("    pantalla los números del 1 al 3.");
     System.out.println(" ");
     for (int i = 1; i <= 3; i++) {
      
      System.out.println(i);
      
    }//Fin del for
     
   }//Fin del método Ejercicio37
   
   public void Ejercicio38(){
     
     System.out.println("38. Escribir un programa en Java que imprima por ");
     System.out.println("    pantalla los números del 1 al 9.");
     System.out.println(" ");
     for (int i = 1; i <= 9; i++) {
      
      System.out.println(i);
      
    }//Fin del for
     
   }//Fin del método Ejercicio38
   
   public void Ejercicio39(){
     
     System.out.println("39. Escribir un programa en Java que imprima por pantalla ");
     System.out.println("    los números del 1 al 10.000.");
     System.out.println(" ");
     for(int i = 1; i <= 10_000; i++){
      
      System.out.println(i);

    }//Fin del for
     
   }//Fin del método Ejercicio39
   
   public void Ejercicio40(){
     
     System.out.println("40. Escribir un programa en Java que imprima por pantalla ");
     System.out.println("    los números del 5 al 10.");
     System.out.println(" ");
     for(int i = 5; i <= 10; i++){
       
       System.out.println(i);
       
     }//Fin del for
     
   }//Fin del método Ejercicio40
   
   public void Ejercicio41(){
     
     System.out.println("41. Escribir un programa en Java que imprima por pantalla ");
     System.out.println("    los números del 5 al 15.");
     System.out.println(" ");
     for(int i = 5; i<= 15; i++){
       
       System.out.println(i);
       
     }//Fin del for 
     
   }//Fin del método Ejercicio41
   
   public void Ejercicio42(){
     
     System.out.println("42. Escribir un programa en Java que imprima por pantalla ");
     System.out.println("    los números del 5 al 15.000.");
     System.out.println(" ");
     for(int i = 5; i<= 15_000; i++){
       
       System.out.println(i);
       
     }//Fin del for
     
   }//Fin del método Ejercicio42
   
   public void Ejercicio43(){
     
       System.out.println("43. Escribir un programa en Java que imprima 200 veces ");
       System.out.println("    la palabra -> hola");
       System.out.println(" ");
       
     for(int i = 1; i <= 200; i++){
       
       
       System.out.printf("hola --> %d\n",i);
       
     }//Fin del for
     
   }//Fin del método Ejercicio43
   
   public void Ejercicio44(){
     
     int cuadrados;
     
      System.out.println("44. Escribir un programa en Java que imprima por pantalla ");
      System.out.println("    los cuadrados de los números del 1 al 30.");
      System.out.println(" ");
      
      for(int i = 1; i <= 30; i++){
        
        cuadrados = (int)Math.pow(i,2);
        System.out.printf("%d^2 --> %d\n",i,cuadrados);
        
      }//Fin del for
     
   }//Fin del método Ejercicio44
   
   public void Ejercicio45(){
     
     int multiplicacion;
     
     multiplicacion = 1;
     
     System.out.println("45. Escribir un programa en Java que multiplique los 20 ");
     System.out.println("    primeros número naturales(1*2*3*4*5...).");
     System.out.println(" ");
     
     for(int i = 1; i<= 20; i++){
       
       multiplicacion = multiplicacion * i; 
       System.out.printf("%d -- >%d\n",i,multiplicacion);
       
     }//Fin del for
     
   }//Fin del método Ejercicio45
   
   public void Ejercicio46(){
     
     int cuadrados;
     int suma = 0;
     
     System.out.println("46. Escribir un programa en Java que sume los cuadrados ");
     System.out.println("    de los cien primeros números naturales, mostrando el resultado ");
     System.out.println("    en pantalla.");
     System.out.println(" ");
     
     for (int i = 1; i <= 100; i++) {
       
       cuadrados = (int)Math.pow(i,2);
       suma = suma + cuadrados;
//       System.out.println(cuadrados);
       
     }//Fin del for
     
     System.out.printf("La suma, de los cuadrados es de: %d\n",suma);
     
   }//Fin del método Ejercicio46
   
   public void Ejercicio47(){
     
     Scanner entrada = new Scanner(System.in);
    int numero;
    int suma=0;
    
    System.out.println("47. Escribir un programa en Java que lea un número ");
     System.out.println("   entero desde teclado y realiza la suma de los 100 ");
     System.out.println("   número siguientes, mostrando el resultado en pantalla. ");
     System.out.println("   (Ejemplo: el usuario digita 5, se debe sumar 5+6+7+8+...");
     System.out.println("   hasta que complete 100 números).");
     System.out.println(" ");
    System.out.println("Ingrese un número");
    numero = entrada.nextInt();
    
    System.out.println("-----------");
    
    for (int i = numero; i <= 100; i++) {
      
      suma += i;

      System.out.println(i);
     
    }//Fin del for
   
    System.out.printf("Resultado %d\n",suma);
     
   }//Fin del método Ejercicio47
   
   public void Ejercicio48(){
     
    Scanner entrada = new Scanner(System.in);
    int numero;

    System.out.println("48. Escribir un programa en Java que lea un número entero ");
    System.out.println("    por el teclado e imprima todos los números impares menores ");
    System.out.println("    que él.");
     System.out.println(" ");
    System.out.println("Ingrese un número");
    numero = entrada.nextInt();

    System.out.println("----------");

    for (int i = numero; i >= 0; i--) {

      if (i % 2 != 0) {

        System.out.println(i);

      }//Fin if

    }//Fin del for
     
   }//Fin del método Ejercicio48
   
   public void Ejercicio49(){
     
    Scanner entrada = new Scanner(System.in);
    int numeroFac;
    int multiplicacion = 1;
    
    System.out.println("49. Halle el número factorial de un número ingresado por el usuario.");
    System.out.println(" ");
    System.out.println("Ingrese un número para hallar su factorial");
    numeroFac = entrada.nextInt();

    System.out.println("------------");

    while (numeroFac < 0) {

      System.out.println("****************************");
      System.out.println("*  ERROR! Número invalido  *");
      System.out.println("****************************");
      System.out.println("--> Ingrese un nuevo Valor!");
      numeroFac = entrada.nextInt();

    }//Fin del while

    for (int i = 1; i <= numeroFac; i++) {

      multiplicacion *= i;

      System.out.println(multiplicacion);

    }//Fin del for

    System.out.printf("El factorial de: %d%s%d\n", numeroFac, "! = ", multiplicacion);
     
   }//Fin del método Ejercicio49
   
   public void Ejercicio50(){
     
    Scanner entrada = new Scanner(System.in);
    int gradosF;
    int entrar = 0;
    double gradosC;
    System.out.println("50. Escriba un programa que lea temperaturas expresadas ");
    System.out.println("    en grados Fahrenheit y las convierta a grados Celsius ");
    System.out.println("    mostrándola. El programa finalizará cuando lea un valor ");
    System.out.println("    de temperatura igual a 999. La conversión de grados Fahrenheit ");
    System.out.println("    (F) a Celsius (C) está dada por C = 5/9(F - 32).");
    System.out.println(" ");
    System.out.println("**************************************");
    System.out.println("* Grados Fahrenheit a Grados Celsius *");
    System.out.println("**************************************");
    System.out.println("--> 999 para Salir! :)");
    
    while(entrar == 0){
      
      System.out.println("-- > Ingrese el valor Fahrenheit");
      gradosF = entrada.nextInt();
      
      while(gradosF >= 1000 || gradosF <= -999){
        
        System.out.println("*******************");
        System.out.println("*      ERROR!     *");
        System.out.println("*******************");
        System.out.println("--> Ingrese un valor mayor a -999 o menor a 999");
        gradosF = entrada.nextInt();
        
      }//Fin del while
      
      if (gradosF < 999) {
       
      //Formula  
      gradosC = (gradosF - 32) /1.8;
      
      System.out.println("");
      System.out.printf("%s %d %s%f%s\n","(",gradosF,"°F - 32) x 5/9 = ",gradosC," °C");
      System.out.println("");
      
      }//Fin del if
      
      if (gradosF == 999) {
        
        entrar = 1;
        System.out.println("*******************");
        System.out.println("* Gracias! Bye... *");
        System.out.println("*******************");
        
      }//Fin del if
     
    }//Fin del while
     
   }//Fin del método Ejercicio50
   
   public void Ejercicio51(){
     
     Scanner entrada = new Scanner(System.in);
    int opc;
    
    System.out.println("51. Implemente una sentencia switch que escriba un ");
    System.out.println("    mensaje en cada caso (10 opciones). Inclúyala en bucle ");
    System.out.println("    de prueba for (5 repeticiones). Utilice también un break ");
    System.out.println("    tras cada caso y pruébelo.");
     System.out.println(" ");
    System.out.println("Ingrese un número!");
    opc = entrada.nextInt();
    
    for (int i = 1; i <= 5; i++) {
      
    switch (opc) {
      
      case 1:
        System.out.println("Mensaje Uno");
        break;
        
      case 2:
        System.out.println("Mensaje Dos");
        break;
        
      case 3:
        System.out.println("Mensaje Tres");
        break;
        
      case 4:
        System.out.println("Mensaje Cuatro");
        break;
        
      case 5:
        System.out.println("Mensaje Cinco");
        break;
        
      case 6:
        System.out.println("Mensaje Seis");
        break;
        
      case 7:
        System.out.println("Mensaje Siete");
        break;
        
      case 8:
        System.out.println("Mensaje Ocho");
        break;
        
      case 9:
        System.out.println("Mensaje Nueve");
        break;
        
      case 10:
        System.out.println("Mensaje Diez");
        break;
        
        }//Fin del for

    }//Fin del switch
     
   }//Fin del método Ejercicio51
   
   public void Ejercicio52(){
     
    int div;
    int raiz;

    Scanner entrada = new Scanner(System.in);
    int numero;
    
    System.out.println("52. Imprima los números primos hasta un número ingresado ");
    System.out.println("    por el usuario.");
    System.out.println(" ");
    System.out.println("Ingrese el número");
    numero = entrada.nextInt();
  
    for (int i = 2;i <= numero;i++) { //ciclo para recorrer los numeros hasta el num 100
    
    div=0; //variable para contar cuantas veces es el residuo de dividir es 0
    raiz=(int)sqrt(i);//la raiz del número a buscarle los primos
  
    for (int j = 1;j <= raiz;j++) { //ciclo para recorrer los numeros hasta la raiz de i (estos seran los divisores)
    
      if (i % j == 0){//evalua la condicion de que el residuo de dividir i entre j es igual a cero
        
        div++;// si la condicion anterior se cumple entonces entonces suma 1 a esta variable
      
      }//Fin del if
      
      }//Fin del for
      
      if (div <= 1){//Si existe más de un divisor, entonces el número no es primo
      
      System.out.println (i);// imprime que cierto numero es primo
      
      }//Fin del if
      
    } //Fin del for
     
     
   }//Fin del método Ejercicio52
  
   public void Ejercicio53(){
     
    Scanner entrada = new Scanner(System.in);
    int numero;
    int multiplicacion;
    
    System.out.println("53. Muestre por pantalla la tabla de multiplicar que el ");
    System.out.println("    usuario desee.");
    System.out.println("");
    System.out.println("Ingrese la tabla de mutiplicar que desea visualizar");
    numero = entrada.nextInt();
    
    for (int i = 1; i <= 10; i++) {
      
      multiplicacion = i * numero;
      System.out.printf("%d%s%d%s%d\n",i," * ",numero," = ",multiplicacion);
      
    }// Fin del for
     
   }//Find el método Ejercicio53
   
   public void Ejercicio54(){
     
     System.out.println("54. Construya el algoritmo que permita generar e imprimir ");
     System.out.println("    los múltiplos de 6 menores que 100 ");
     System.out.println(" ");
     for (int i = 0; i <= 100; i++) {
      
      if (i % 6 == 0) {
        
        System.out.println(i);
        
      }//Fin del if
      
    }//Fin del for
     
   }//Fin del método Ejercicio54
   
   public void Ejercicio55(){
     
    Scanner entrada = new Scanner(System.in);
    int numero;
    int contadorUno = 0;
    int contadorDos = 0;
    int contadorTres = 0;
    
    
    System.out.println("55. Construya el algoritmo que permita el ingreso de una ");
    System.out.println("    serie de números y que determine cuantos números positivos, ");
    System.out.println("    cuantos números negativos y cuantos ceros hay. Como primer ");
    System.out.println("    dato pedir la cantidad de números que forman la lista.");
    System.out.println(" ");
    System.out.println("Ingrese la cantidad de números que quiere evaluar");
    numero = entrada.nextInt();
    
    for (int i = 1; i <= numero; i++) {
      
      System.out.printf("Ingrese el Número %d\n",i);
      int dato = entrada.nextInt();
      
      if (dato == 0) {
        
        contadorUno++;
        
      }//Find contadorUno
      
      if (dato < 0) {
        
        contadorDos++;
        
      }//Fin del contadorDos
      
      if (dato > 0) {
        
        contadorTres++;
        
      }//Fin del contadorTres
      
    }//Fin del for
    
    System.out.println("");
    System.out.printf("Cantidad de números positivos: %d\n",contadorTres);
    System.out.printf("Cantidad de números Negativos: %d\n",contadorDos);
    System.out.printf("Cantidad de ceros : %d\n",contadorUno);
     
   }//Fin del método Ejercicio55
   
   public void Ejercicio56(){
     
    Scanner entrada = new Scanner(System.in);
    
    int entrar = 0;
    int salir = 0;
    
    int numero;
    int suma = 0;
    int contador = 0;
    int promedio;
    
    System.out.println("56. Se dispone de un conjunto de números positivos. ");
    System.out.println("    Calcular y escribir su promedio sabiendo que se ingresará ");
    System.out.println("    un valor menor a 0 para indicar el fin del conjunto de valores ");
    System.out.println(" ");
    while(entrar == 0 || salir == 0){
      
      System.out.println("Ingrese números positivos o un numero negativo para Salir!");
      numero = entrada.nextInt();
      
      if (numero >= 0 ) {
        
        contador++;
        suma = suma + numero;
          
      }//Fin del if
      
      if (numero < 0) {
        
        entrar = 1;
        salir = 1;
        
      }//Fin if 
      
    }//Fin del while
    
    promedio = suma/contador;
    
    System.out.printf("El promedio es de: %d\n",promedio);
     
   }//Fin del método Ejercicio56
   
   public void Ejercicio57(){
     
    Scanner entrada = new Scanner(System.in);
    int numeroRectangulo;
    int base = 0;
    int altura = 0;
    int superficie;
    int perimetro;
    
    System.out.println("57. Calcular el perímetro y la superficie de N rectángulos ");
    System.out.println("    pidiendo la base y la altura.");
    System.out.println(" ");
    System.out.println("ingrese la cantidad de rectangulos que require evaluar");
    numeroRectangulo = entrada.nextInt();
    
        for(int contador = 1; contador <= numeroRectangulo; contador++){
          
         int entrar =0;
         while(entrar == 0){
           
            System.out.println("Ingrese la base del rectangulo");
            base = entrada.nextInt();
            System.out.println("Ingrese la altura del trinagulo");
            altura = entrada.nextInt();
            
            if (base != altura) {
              
              entrar = 1;
             
           }//Fin del if
            
            if(base == altura){
              
           System.out.println("********************************");
           System.out.println("* ERROR! NO pueden ser IGUALES *");
           System.out.println("********************************");
              
              entrar = 0;
              
            }//Fin del if
         
         }//Fin del while
      
         superficie = base * altura;
          
         System.out.printf("La superficie del rectangulo es: %d\n",superficie);
         
         perimetro = (base + base) + (altura + altura);
         
         System.out.printf("El perimetro del rectangulo es: %d\n",perimetro);
         
        }//fin del for
  
   }//Fin del método Ejercicio57
   
   public void Ejercicio58(){
     
    int num = 1;
    int fact = 0;
    
    System.out.println("58. Hacer un programa que nos dé una tabla de los números ");
    System.out.println("    factoriales de los diez primeros números naturales.");
    System.out.println(" ");
    for (int i = 1; i <= 10; i++) {
      
      num = num * i;
      System.out.printf("factorial de %d%s%d\n",i,"! = ",num);

    }//Fin del for
     
   }//Fin del método Ejercicio58
   
   public void Ejercicio59(){
     
     int num;
    System.out.println("59. Hacer un programa que muestre las tablas de multiplicar ");
    System.out.println("    del 1 al 9. Cada tabla debe tener su título.");
    System.out.println(" ");
    for(int i=1; i<=9; i++){
      
      switch(i){
          
          case 1:
            System.out.println("Tabla del 1");
            break;
            
            case 2:
            System.out.println("Tabla del 2");
            break;
            
            case 3:
            System.out.println("Tabla del 3");
              break;
              
            case 4:
            System.out.println("Tabla del 4");
              break;
              
            case 5:
            System.out.println("Tabla del 5");
              break;
              
            case 6:
            System.out.println("Tabla del 6");
              break;
              
            case 7:
            System.out.println("Tabla del 7");
              break;
              
            case 8:
            System.out.println("Tabla del 8");
              break;
              
            case 9:
            System.out.println("Tabla del 9");
              break;
             
        }//Fin de switch
      
      for(int j = 1; j<=10; j++){
        
        num = i * j;
       
        System.out.printf(" %d %s %d %s %d\n",i,"X",j,"=",num);
        
        
        
        if (j==10) {
          
          System.out.println("");
          
        }//Fin del if
 
      } //fin del for 1 
      
    }//fin del for 2
     
   }//Fin del método Ejercicio59
   
   public void Ejercicio60(){
     
    int suma;
    int valores;

    Scanner entrada = new Scanner(System.in);
    
    System.out.println("60. Pedir sucesivamente 20 valores numéricos. A cada valor ");
    System.out.println("    multiplicarlo por 3 y sumarle 5. Mostrar el retorno de dicha ");
    System.out.println("    expresión junto con el número que la origina. Al final mostrar ");
    System.out.println("    el valor acumulado.");
    System.out.println("");
    System.out.println("************************************");
    System.out.println("* Ingrese 20 valores sucesivamente *");
    System.out.println("************************************");
  
    for (int i = 1; i <= 20; i++) {
      
      System.out.printf("-- > Ingrese el número %d\n",i);
      valores = entrada.nextInt();

      suma = (valores * 3) + 5;

      System.out.printf("Número original: %d%s%d\n",valores," acumulado: ",suma);
      System.out.println("");

    }// fin for
     
   }//Fin del método Ejercicio60
   
   public void Ejercicio61(){
     
   int numero1;
   int numero2;
   
   Scanner entrada = new Scanner(System.in);
   
   System.out.println("61. Dados dos números naturales, el primero menor al segundo, ");
   System.out.println("    generar y mostrar todos los números comprendidos entre ellos ");
   System.out.println("    en secuencia ascendente.");
   System.out.println(" ");
   System.out.println("--> Ingrese el Primer Número");
   numero1 = entrada.nextInt();
//   int entrar = 0;
   while(numero1 < 0){
     
     System.out.println("*****************************");
     System.out.println("* Error! número NEGATIVO... * ");
     System.out.println("*****************************");
     System.out.println("--> Ingrese el Número Uno...");
     numero1 = entrada.nextInt();
     
   }//Fin del while
   
   System.out.println("--> Ingrese el Segundo Número");
   numero2 = entrada.nextInt();
   
   while(numero2 < 0){
     
     System.out.println("*****************************");
     System.out.println("* Error! número NEGATIVO... * ");
     System.out.println("*****************************");
     System.out.println("--> Ingrese el Número Dos...");
     numero2 = entrada.nextInt();
     
   }//Fin del while
   
    while(numero2 <= numero1){
      
      System.out.println("******************************************");
      System.out.println("* ERROR!, el segundo número no puede ser *");
      System.out.println("*         menor o igual al primero       *");
      System.out.println("******************************************");
      System.out.println("--> Ingrese el número 2");
      numero2 = entrada.nextInt();
      
   }//Fin while
    
       for(int i = numero1 +1; i < numero2; i++){
       
       System.out.printf("--> Secuencia: %d\n",i);
      
      }//fin if 
     
   }//Fin del método Ejercicio61
   
   public void Ejercicio62(){
     
     int cubo;
    
    System.out.println("62. Diseñar un algoritmo que escriba el cubo de los números ");
    System.out.println("    del 1 al 20.");
    System.out.println(" ");
    for(int kk = 1; kk <= 20; kk++){
      
      cubo = (int)Math.pow(kk,3);
      System.out.printf("%d%s%d\n",kk,"^3 = ",cubo);
      
    }//Fin del for
     
   }//Fin del método Ejercicio62
   
   public void Ejercicio63(){
     
    int cubo;
    int aux;
    int contador = 0;
    int original;
    
     System.out.println("63. Diseñar un algoritmo que escriba el cubo de los ");
     System.out.println("    números naturales tales que el cubo tenga como máximo ");
     System.out.println("    cuatro cifras.");
     System.out.println(" ");
    for (int i = 1; i <= 9999; i++) {
      
      cubo = i * i * i;
      original = cubo;
    
      while(cubo != 0){
        
        cubo = cubo/10;
        contador++;
       
      }//Fin while
     
      if (contador > 3 && contador < 5) {
        
        System.out.printf("%d%s%d\n",i," - ",original);
        
      }//Fin del if
      
      contador = 0;
      
    }//Fin del for
     
   }//Fin del método Ejercicio63
   
   public void Ejercicio64(){
     
     System.out.println("64. Realizar un algoritmo que muestre los valores de ");
     System.out.println("    todas las piezas del domino de forma ordenada: 0-0 0-1 ");
     System.out.println("    1-1 0-2 1-2 2-2");
     System.out.println(" ");
     
     for (int i = 0; i <= 6; i++) {
     
      for (int j = i; j <= 6; j++) {
        
        System.out.printf("%d%s%d\n", i, " - ", j);

       if (j == 6) {
        
        System.out.println("");
        
      }//Fin if
        
      }//Fin for
      
    }//Fin del for
     
   }//Fin del método Ejercicio64
   
   public void Ejercicio65(){
     
     int multiplicacion;
      
     System.out.println("65. Realizar un algoritmo que muestre por pantalla la "); 
     System.out.println("    tabla de multiplicar del dos. Hacer tres versiones ");
     System.out.println("    utilizando en cada una de ellas cada una de las estructuras ");
     System.out.println("    repetitivas (for, while, do while).");
     System.out.println(" ");
     
      System.out.println("Tabla del 2 usando FOR");
      for (int i = 1; i <= 10; i++) {
        
        multiplicacion = i * 2;
        System.out.printf("%d%s%d\n",i," * 2 =",multiplicacion);
        
      }//Fin del for
      
      System.out.println("");
     System.out.println("Tabla del 2 usando WHILE");
      int entrar = 0;
      int contador = 0;
      int numero;
      int resultado;
      
         while(entrar == 0){
        
        contador++;
        numero = contador;
        resultado = numero * 2;
        System.out.printf("%d%s%d\n",numero," * 2 = ",resultado);
        
        if(contador == 10) {
          
          entrar = 1;
          
        }//Fin if
        
      }//Fin del while  
      
      
    //TABLA DEL 2 USANDO DO WHILE
    System.out.println("");
    System.out.println("Tabla del 2 usando DO WHILE");
    
    contador = 0;
    
    do{
    
        contador++;
        numero = contador;
        resultado = numero * 2;
        
         System.out.printf("%d%s%d\n",numero," * 2 = ",resultado);
        
        if(contador == 10) {
          
          entrar = 0;
          
        }//fin del IF
     
    } while(entrar == 1);
     
   }//Fin del método Ejercicio65
   
   public void Ejercicio66(){
     
    Scanner entrada = new Scanner(System.in);
    int numeroUno;
    int numeroDos;
    int cociente;
    
    System.out.println("66. Realizar un algoritmo que determine el valor del cociente ");
    System.out.println("    y el resto de una división entre números enteros ingresados ");
    System.out.println("    por el usuario.");
    System.out.println(" ");
    System.out.println("Ingrese el Número del Dividendo");
    numeroUno = entrada.nextInt();
    
    while(numeroUno <= 0){
      
      System.out.println("**********************************************");
      System.out.println("* Error! el dividendo NO! puede ser Negativo *");
      System.out.println("**********************************************");
      System.out.println("--> Ingrese un Dividendo... Valido!");
      numeroUno = entrada.nextInt();
      
    }//Fin del While
    
    System.out.println("Ingrese el Número del Divisor");
    numeroDos = entrada.nextInt();
    
     while(numeroDos <= 0){
      
      System.out.println("**********************************************");
      System.out.println("* Error! el divisor NO! puede ser Negativo *");
      System.out.println("**********************************************");
      System.out.println("--> Ingrese un Divisor... Valido!");
      numeroDos = entrada.nextInt();
      
    }//Fin del While
     
     cociente = numeroUno / numeroDos;
     
     System.out.printf("%d%s%d%s%d\n",numeroUno,"/",numeroDos," = ",cociente);
     
   }//Fin del método Ejercicio66
   
   public void Ejercicio67(){
     
    Scanner entrada = new Scanner(System.in);
    int numero;
    int imprime = 0;
    int mas = 1;
    int resultado;
    
     System.out.println("67. Dada la serie de números naturales de Fibonacci: ");
     System.out.println("    Sucesión: 1, 1, 2, 3, 5, 8, 13, 21,... diseñar un algoritmo ");
     System.out.println("    que pida un número natural y calcule e imprima la serie hasta ");
     System.out.println("    el número ingresado.");
     System.out.println(" ");
    
    System.out.println("Ingrese un Número, Por favor!");
    numero = entrada.nextInt();
    
    for (int i = 1; i <= numero; i++) {
      
      System.out.printf("%d%s%d\n",i," - ",imprime);
      resultado = imprime + mas;
      mas = imprime;
      imprime = resultado;
      
    }//Fin del for
     
   }//Fin del método Ejercicio67
   
   public void Ejercicio68(){
     
    Scanner entrada = new Scanner(System.in);
    int numero;
    int numeroDos;
    int promedio;
    int contadorUno = 0;
    int contadorDos = 0;
    int sumaUno = 0;
    int sumaDos = 0;
    int promedioUno;
    int promedioDos;
    
    System.out.println("68. Se recibe una lista de números. Calcular y mostrar ");
    System.out.println("    el promedio de los valores positivos y el promedio de los ");
    System.out.println("    negativos.");
     System.out.println(" ");
    System.out.println("Ingrese el Número que desea a evaluar");
    numero = entrada.nextInt();
    
    for (int i = 1; i <= numero; i++) {
      
      System.out.println("Ingrese el Número");
      numeroDos = entrada.nextInt();
      
      if (numeroDos < 0) {
        
        contadorUno++;
        sumaUno = sumaUno + numeroDos;
        
      }//Fin del if
      
      if (numeroDos > 0) {
        
        contadorDos++;
        sumaDos = sumaDos + numeroDos;
        
      }//Fin del if
      
    }//Fin del for
    
    if (contadorUno > 0) {
      
      promedioUno = sumaUno / contadorUno;
      System.out.printf("El promedio de los numeros Negativos es de: %d\n",promedioUno);
      
    }//Fin del if
    
    if (true) {
      
      promedioDos = sumaDos / contadorDos;
      System.out.printf("El promedio de los numeros Positivos es de: %d\n",promedioDos);
      
    }//Fin del if
     
   }//Fin del método Ejercicio68
   
   public void Ejercicio69(){
     
    Scanner entrada = new Scanner(System.in);
    int suma = 0;
    int numero;
    int entrar = 0 ;
    int salir = 0;
    
    System.out.println("69. Sumar todos los números que se introducen mientras no sea 0.");
    System.out.println(" ");
    while(entrar == 0 || salir == 0){
      
      System.out.println("Ingrese un Número");
      numero = entrada.nextInt();
      
      suma = suma + numero;
      
      if (numero == 0) {
        
        entrar = 1;
        salir = 1;
        
      }//Fin del if
      
    }//Fin del while
    
    System.out.printf("La suma de los Números INGRESADOS es de: %d\n",suma);
     
   }//Fin del método Ejercicio69
   
   public void Ejercicio70(){
     
     System.out.println("70. Pedir los datos de los alumnos, estos son: sexo, edad ");
     System.out.println("    y altura. Al final del programa se deberá mostrar la ");
     System.out.println("    cantidad de varones, la cantidad de mujeres, la altura ");
     System.out.println("    promedio y la finalizar cuando la edad sea igual a 0.");
     System.out.println(" ");
     
     Scanner entrada = new Scanner(System.in);
    int sexo;
    int edad;
    double altura;
    int alumnos;
    int entrar;
    int contadorAlturaPro = 0;
    int contadorAltura = 0;
    int contadorHombres = 0;
    int contadorMujeres = 0;
    int alturaPro;
    double  sumaAltura = 0;
    double promedioAltura;
    
    entrar = 0 ;
    
    while(entrar == 0){
      
      System.out.println("***************************************************");
      System.out.println("--> Ingrese el SEXO 1-Hombres 2-Mujeres o 0-Salir *");
      System.out.println("***************************************************");
      sexo = entrada.nextInt();
      
      if(sexo == 0){
        
        entrar = 1;
        
      }//Fin if Salir
      
      if(sexo > 0){
      
      while(sexo < 0 || sexo > 2){
        
        System.out.println("***************************");
        System.out.println(" ERROR! Opción IVALIDA...");
        System.out.println("***************************");
        sexo = entrada.nextInt();
        
      }//Fin del while del sexo
      
      if(sexo == 1){
        
        contadorHombres++;
        
      }//Contador Hombres
      
      if(sexo == 2){
        
        contadorMujeres++;
        
      }//Contador Mujeres
      
      System.out.println("********************");
      System.out.println("--> Ingrese la Edad");
      edad = entrada.nextInt();
      
      while(edad <= 0 || edad >= 100){
        
        System.out.println("***************************");
        System.out.println(" ERROR! Edad IVALIDA...");
        System.out.println("***************************");
        edad = entrada.nextInt();
        
      }//Fin del While de edad
      
      System.out.println("**************************");
      System.out.println("--> Ingrese la Altura (,)");
      altura = entrada.nextDouble();
      
      while(altura <= 0 || altura >= 3){
        
        System.out.println("***************************");
        System.out.println(" ERROR! Altura IVALIDA...");
        System.out.println("***************************");
        altura = entrada.nextDouble();
        
      }//Fin del While de altura
      
      if(altura > 0){
        
        contadorAltura++;
        sumaAltura = altura + sumaAltura;
        
      }//Fin if aluraPromedio
      
      if(altura > 1.50){
        
        contadorAlturaPro++;
        
      }//Fin If altura promedio
      
      }//Fin del Segundo IF! que valida que el sexo es > a 0
      
    }//Fin del While Principal
    
    System.out.println(" ");
    promedioAltura = (double)sumaAltura / contadorAltura;
    System.out.printf("Total de Hombres: %d\n",contadorHombres);
    System.out.printf("Total de Mujeress: %d\n",contadorMujeres);
    System.out.printf("La altura Promedio es de: %.2f\n",promedioAltura);
    System.out.printf("Alumnos que miden mas de 1.50 :%d\n ",contadorAlturaPro);
    
     
     
   }//Fin del método Ejericicio70
   
   public void Ejercicio71(){
     
    System.out.println("71. Ingresar como datos las temperaturas registradas ");
    System.out.println("    durante todo el día a intervalo de media hora comenzando ");
    System.out.println("    desde la hora 0. Determinar las horas en la cual se ");
    System.out.println("    produjo la temperatura mínima y la máxima.");
    System.out.println(" ");
     
    Scanner entrada = new Scanner(System.in);
    int temperatura = 0;
    int mediaHora;
    int min = 9999;
    int max = 0;
    int opc;
    
    int hora = 0;
    int minuto = 0;
    
    int horaTemperaturaMax = 0;
    int minutoTemperaturaMax = 0;
    
    int horaTemperaturaMin = 0;
    int minutoTemperaturaMin = 0;
    
//    int horaTemperaturaMax;
//    int minTemperaturaMax;
    
    for (int i = 0; i <= 48; i++) {
     
        opc = i;

              switch(opc){

                case 0:
                  hora = 0;
                  minuto = 0;
                  break;

                case 1:
                  hora = 0;
                  minuto = 30;
                  break;

                case 2:
                  hora = 1;
                  minuto = 00;
                  break;

                case 3:
                  hora = 1;
                  minuto = 30;
                  break;

                case 4:
                  hora = 2;
                  minuto = 0;
                  break;


                case 5:
                  hora = 2;
                  minuto = 30;
                  break;

                case 6:
                  hora = 3;
                  minuto = 0;
                  break;
                  
                case 7:
                  hora = 3;
                  minuto = 30;
                  break;
                  
                case 8:
                  hora = 4;
                  minuto = 0;
                  break;
                  
                case 9:
                  hora = 4;
                  minuto = 30;
                  break;
                  
                case 10:
                  hora = 5;
                  minuto = 0;
                  break;
                  
                case 11:
                  hora = 5;
                  minuto = 30;
                  break;
                  
                case 12:
                  hora = 6;
                  minuto = 0;
                  break;
                  
                case 13:
                  hora = 6;
                  minuto = 30;
                  break;
                  
                case 14:
                  hora = 7;
                  minuto = 0;
                  break;
                  
                case 15:
                  hora = 7;
                  minuto = 30;
                  break;
                  
                case 16:
                  hora = 8;
                  minuto = 0;
                  break;
                  
                case 17:
                  hora = 8;
                  minuto = 30;
                  break;
                  
                case 18:
                  hora = 9;
                  minuto = 0;
                  break;
                  
                case 19:
                  hora = 9;
                  minuto = 30;
                  break;
                  
                case 20:
                  hora = 10;
                  minuto = 0;
                  break;
                  
                case 21:
                  hora = 10;
                  minuto = 30;
                  break;
                  
                case 22:
                  hora = 11;
                  minuto = 0;
                  break;
                  
                case 23:
                  hora = 11;
                  minuto = 30;
                  break;
                  
                case 24:
                  hora = 12;
                  minuto = 0;
                  break;
                  
                case 25:
                  hora = 12;
                  minuto = 30;
                  break;
                  
                case 26:
                  hora = 13;
                  minuto = 0;
                  break;
                  
                case 27:
                  hora = 13;
                  minuto = 30;
                  break;
                  
                case 28:
                  hora = 14;
                  minuto = 0;
                  break;
                  
                case 29:
                  hora = 14;
                  minuto = 30;
                  break;
                  
                case 30:
                  hora = 15;
                  minuto = 0;
                  break;
                  
                case 31:
                  hora = 15;
                  minuto = 30;
                  break;
                  
                case 32:
                  hora = 16;
                  minuto = 0;
                  break;
                  
                case 33:
                  hora = 16;
                  minuto = 30;
                  break;
                  
                case 34:
                  hora = 17;
                  minuto = 0;
                  break;
                  
                case 35:
                  hora = 17;
                  minuto = 30;
                  break;
                  
                case 36:
                  hora = 18;
                  minuto = 0;
                  break;
                  
                case 37:
                  hora = 18;
                  minuto = 30;
                  break;
                  
                case 38:
                  hora = 19;
                  minuto = 0;
                  break;
                  
                case 39:
                  hora = 19;
                  minuto = 30;
                  break;
                  
                case 40:
                  hora = 20;
                  minuto = 0;
                  break;
                  
                case 41:
                  hora = 20;
                  minuto = 30;
                  break;
                  
                case 42:
                  hora = 21;
                  minuto = 0;
                  break;
                  
                case 43:
                  hora = 21;
                  minuto = 30;
                  break;
                  
                case 44:
                  hora = 22;
                  minuto = 0;
                  break;
                  
                case 45:
                  hora = 22;
                  minuto = 30;
                  break;
                  
                case 46:
                  hora = 23;
                  minuto = 0;
                  break;
                  
                case 47:
                  hora = 23;
                  minuto = 30;
                  break;
                  
                case 48:
                  hora = 24;
                  minuto = 0;
                  break;
                

              }//Fin switch
        
      System.out.printf("Ingrese la temperatura de las: %d%s:%d\n",hora,"",minuto);
      temperatura = entrada.nextInt();
      
      if (temperatura < min) {
        
            min = temperatura;
            
            horaTemperaturaMin = hora;
            minutoTemperaturaMin = minuto;
          
             }//Fin if
      
        else if(temperatura > max){
          
            max = temperatura;
            
            horaTemperaturaMax = hora;
            minutoTemperaturaMax = minuto;
            
        }//Fin else if
     
    }//Fin del for
    
    
      System.out.printf("la temperatura Máxima es de: %d%s%d%s:%d\n",max," Hora: ",horaTemperaturaMax,"",             minutoTemperaturaMax);
      System.out.printf("la temperatura Minima es de: %d%s%d%s:%d\n",min," Hora: ",horaTemperaturaMin,"",             minutoTemperaturaMin);
     
   }//Fin del método Ejercicio71
   
   public void Ejercicio72(){
     
     Scanner entrada = new Scanner(System.in);
    int numero = 0;
    int ban;
    
    System.out.println("72. Determinar si un número ingresado N es par o impar, ");
    System.out.println("    controlar que el número ingresado sea entero y positivo.");
    System.out.println(" ");
    
    //---------------------------------------------------
    //********* VALIDA QUE EL DATO INGRESADO SEA EL CORRECTO! UFF VALE ORO.
    do{
    try{
      
    ban = 0;
    
    System.out.println("Ingrese un Número para determinar si es PAR o IMPAR");
    numero = entrada.nextInt();
    
    while(numero < 0){
      
      System.out.println("*******************************");
      System.out.println("*  ERROR! número menor a CERO *");
      System.out.println("*******************************");
      System.out.println("--> Ingrese un Número ");
      numero = entrada.nextInt();
      
    }//Fin del while
    
    }catch(Exception e){
    
      System.out.println("Caracter INVALIDO!");
      ban = 1;
      entrada.nextLine();
      
    }//Fin Catch
    }while(ban != 0);
    
    //---------------------------------------------------
    
    if (numero % 2 == 0) {
      
      System.out.printf("El Número %d%s\n",numero," es PAR!");
      
    }//Fin del if
    
    if (numero % 2 != 0) {
      
      System.out.printf("El Número %d%s\n",numero," es IMPAR");
      
    }//Fin del if
     
   }//Fin del método Ejercicio72
   
   public void Ejercicio73(){
     
     System.out.println("73. Escriba un programa en java que genere aleatoriamente ");
     System.out.println("    (Ayuda: Vea la clase Math para saber cómo generar números ");
     System.out.println("    aleatorios en java) un array de números reales, y lo ordene ");
     System.out.println("    mediante un método de ordenamiento (burbuja, selección o ");
     System.out.println("    inserción) de menor a mayor. La cantidad de números será ");
     System.out.println("    definida por el usuario.");
     System.out.println(" ");
     
     Scanner lector = new Scanner(System.in);

        int arr[] = new int[50];
        int dim, i, j, numaleatorio, aux;

        System.out.println("Ingrese el tamaño del arreglo ");
        dim = lector.nextInt();

        //Aqui es el proceso en donde lleno el arreglo con numeros aleatorios
        for (i = 0; i < dim; i++) {
          
            arr[i] = numaleatorio = (int) Math.floor(Math.random() * (100 - 10 + 1) + 10);//Genero numeros aleatorios entre 100 y 10
            
        }//Fin del for
        
        
        System.out.println("Sin ordenar ");
        for( i=0; i < dim; i++){
          
            System.out.print(arr[i]+",");
            
        }//Fin del for
        
        System.out.println(" ");

        for (i = 0; i < dim; i++) {
          
            for (j = i + 1; j < dim; j++) {
              
                //si pones arr[i]<arr[j] se ordenara de mayor a menor
                //y si pones arr[i]>arr[j] se ordenara de menor a mayor solo cambia la condicion de if
                if(arr[i] > arr[j]){
                  
                    aux=arr[i];
                    arr[i]=arr[j];
                    arr[j]=aux;
                    
                }//Fin del if
                    
            }//Fin del for
            
        }//Find el for
        
        //muestro el arreglo ordenado
        System.out.println("\nOrdenado ");
        for( i = 0;i < dim; i++){
          
            System.out.print(arr[i]+",");
            
        }//Fin del for
        
        System.out.println(" ");
     
   }//Fin del método Ejercicio73
   
   public void Ejercicio74(){
     
        
        System.out.println("74. Elabore una aplicación que permita introducir ");
        System.out.println("    20 elementos de tipo entero en un arreglo, el ");
        System.out.println("    programa mostrara impreso el arreglo en orden inverso.");
        System.out.println(" ");
        
        Scanner entrada = new Scanner (System.in);

        int aux;
        int b;
        int[] arreglo = new int[5];

        int ban;

        do{
        try{

            ban = 0; 

            for(int i = 0;i < arreglo.length;i++){

            System.out.print("X["+(i+1)+"]= ");
            arreglo[i] = entrada.nextInt();

            }
            
            // intercambiar 
            System.out.println("Arreglo sin valores intercambiados ");

            for(int j = 0;j < arreglo.length; j++){

            System.out.print(arreglo[j]+", ");

            }//Fin del for

            b = arreglo.length;

            for(int i = 0; i <= b / 2; i++){

            aux = arreglo[i];
            arreglo[i] = arreglo[b-1];
            arreglo[b-1] = aux;
            b--;

            }//Fin del For

            System.out.println("\nArreglo con sus valores intercambiados ");

            for(int i = 0;i < arreglo.length ; i++){

            System.out.print(arreglo[i]+", ");

            }//Fin del for

            System.out.println("");

        }catch(Exception e){

          System.out.println("XXXXXXXXXXXXXXXXXXXX");
          System.out.println(" Caracter INVALIDO!");
          System.out.println("XXXXXXXXXXXXXXXXXXXX");
          ban = 1;
          entrada.nextLine();

        }//Fin Catch
        }while(ban != 0);
     
   }//Fin del método Ejercicio74
   
   public void Ejercicio75(){
     
     System.out.println("75. Hacer un programa que lea diez valores enteros en ");
     System.out.println("    un array y los muestre en pantalla. después que los ");
     System.out.println("    ordene de menor a mayor y los vuelva a mostrar. y finalmente ");
     System.out.println("    que los ordene de mayor a menor y los muestre por tercera vez.");
     System.out.println(" ");
     
     Scanner entrada = new Scanner(System.in);

        int []arreglo = new int[5];
        int numero, i, j,  aux;


        //Aqui es el proceso en donde lleno el arreglo con numeros aleatorios
        for (i = 0; i < arreglo.length; i++) {
          
            System.out.println("Ingrese el Número...");
            arreglo[i] = entrada.nextInt();
        }
        
        System.out.println("Arreglo sin ordenar ");
        
        for(i=0; i < arreglo.length; i++){
          
            System.out.print(arreglo[i]+",");
            
        }//Fin del for
        
        System.out.println(" ");
        
         for (i = 0; i < arreglo.length; i++) {
          
            for (j = i + 1; j < arreglo.length; j++) {
                //si pones arr[i]<arr[j] se ordenara de mayor a menor
                //y si pones arr[i]>arr[j] se ordenara de menor a mayor solo cambia la condicion de if
                
                if(arreglo[i]>arreglo[j]){
                  
                    aux=arreglo[i];
                    arreglo[i]=arreglo[j];
                    arreglo[j]=aux;
                    
                }//Fin if
                    
            }//Fin for
            
        }//Fin for
         
          //muestro el arreglo ordenado
        System.out.println("\nArreglo ordenado Menor a Mayor");
        for(i=0;i<arreglo.length;i++){
            System.out.print(arreglo[i]+",");
        }
          
        System.out.println(" ");
        
        for (i = 0; i < arreglo.length; i++) {
          
            for (j = i + 1; j < arreglo.length; j++) {
                //si pones arr[i]<arr[j] se ordenara de mayor a menor
                //y si pones arr[i]>arr[j] se ordenara de menor a mayor solo cambia la condicion de if
                
                if(arreglo[i]<arreglo[j]){
                  
                    aux=arreglo[i];
                    arreglo[i]=arreglo[j];
                    arreglo[j]=aux;
                    
                }//Fin if
                    
            }//Fin del for
            
        }//Fin del for
        
        //muestro el arreglo ordenado
        System.out.println("\nArreglo ordenado Mayor a Menor");
        
        for(i=0;i<arreglo.length;i++){
          
            System.out.print(arreglo[i]+",");
            
        }//Fin del for
        
        System.out.println("");
     
   }//Fin del método Ejercicio75
   
   public void Ejercicio76(){
     
     System.out.println("76. Elabore un programa que encuentre al número mayor ");
     System.out.println("    y menor de un arreglo y luego muestre en qué posición ");
     System.out.println("    se encontraban estos números originalmente.");
     System.out.println(" ");
     
    Scanner entrada = new Scanner(System.in);
    int numero;
    int ban;
    
    do{
    try{
      
    ban =0;
    
    System.out.println("Ingrese el valor de la dimension del Arreglo");
    numero = entrada.nextInt();
    
    int[] arreglo = new int[numero];
    
        for (int i = 0; i < arreglo.length; i++) {
          
          System.out.print("X["+(i+1)+"]= ");
          arreglo[i] = entrada.nextInt();
      
        }//Find el For
        
        int minimo = arreglo[0];
        int maximo = arreglo[0];
        int posicionMin = 0;
        int posicionMax = 0;
        
        for (int i = 0; i < arreglo.length; i++) {
          
          if (arreglo[i] > maximo) {
           
            maximo = arreglo[i];
            posicionMax = i;
            
          }//Fin if
          
          if (arreglo[i] < minimo) {
            
            minimo = arreglo[i];
            posicionMin = i;
            
          }//Fin del if Minimo
          
        }//Fin del For

        posicionMax += 1;
        posicionMin += 1;
        
        
        System.out.printf("Valor Máximo: %d%s%d%s\n",maximo," - Posición[",posicionMax,"]");
        System.out.printf("Valor minimo: %d%s%d%s\n",minimo," - Posición[",posicionMin,"]");
       
    }catch(Exception e){
    
      System.out.println("xxxxxxxxxxxxxxxxxxxxxxxxxxxx");
      System.out.println("  Ups! Caracter INVALIDO!");
      System.out.println("xxxxxxxxxxxxxxxxxxxxxxxxxxxx");
      ban = 1;
      entrada.nextLine();
      
    }//Fin Catch
    }while(ban != 0);
     
   }//Fin del método Ejercicio76
   
   public void Ejercicio77(){
     
    System.out.println("77. Elabore un programa que imprima en orden inverso ");
    System.out.println("    una cadena de caracteres.");
    System.out.println(" ");
     
    String cadena = "";
    String cadenaInvertida = " ";
    Scanner entrada = new Scanner(System.in);
    
    System.out.println("Ingrese la cadena de caracteres...");
    cadena = entrada.nextLine();
    
    for (int i = cadena.length()-1; i >= 0; i--) {
     
      cadenaInvertida += cadena.charAt(i);
      
    }//Fin for
    
    System.out.println("Cadena Invertida = "+cadenaInvertida);
     
   }//Fin del método Ejercicio77
   
   public void Ejercicio78(){
     
     System.out.println("78. elabore un programa que permita introducir un arreglo ");
     System.out.println("    de 10 elementos, el programa mostrara un histograma de esos ");
     System.out.println("    datos (el histograma se interpretara con la salida de n  ");
     System.out.println("    asteriscos donde n es el valor de cada elemento del arreglo) ");
     System.out.println("    ej.: el arreglo es 2,3,4 el histograma será 2->** 3->*** 4->****");
     System.out.println(" ");
     
     Scanner entrada = new Scanner(System.in);
      int[] arreglo = new int[5];
      int numero;
      int n;
      int ban;
    
      do{
      try{

      ban =0;
      
      n = arreglo.length;

      for (int i = 0; i < arreglo.length; i++) {
        
        System.out.printf("Ingrese el número %d\n",i+1);
        arreglo[i] = entrada.nextInt();

      }//Fin del For
      
      System.out.println(" ");
    
      for (int i = 0; i < arreglo.length; i++) {
        
        System.out.print(arreglo[i]+"-->");
        
        for (int j = 1; j <= arreglo[i]; j++) {
          
          System.out.print("*");
          
        }//Fin for asteriscos
        
        System.out.println("");
        
      }//Fin del for
      
      }catch(Exception e){

          System.out.println("XXXXXXXXXXXXXXXXXXXX");
          System.out.println(" Caracter INVALIDO!");
          System.out.println("XXXXXXXXXXXXXXXXXXXX");
          ban = 1;
          entrada.nextLine();

        }//Fin Catch
        }while(ban != 0);
        
     
   }//Fin del método Ejercicio78
   
   public void Ejercicio79(){
     
     System.out.println("79. Elabore un programa que permita introducir un ");
     System.out.println("    arreglo de 25 elementos de tipo entero. luego pedir ");
     System.out.println("    al usuario que introduzca un número. el programa ");
     System.out.println("    mostrara el número de veces que se repite dicho valor ");
     System.out.println("    en el arreglo");
     System.out.println(" ");
     
    Scanner entrada = new Scanner(System.in);
    Random numerosAleatorios = new Random();
    int[] arreglo = new int[25];
    int numero;
    int contador = 0;
    int posicion = 0;
    int ban;

        do{
        try{

            ban = 0;
    
          System.out.println("");

          for (int i = 0; i <= 25; i++) {

            ++arreglo[numerosAleatorios.nextInt(25)];

          }//Fin del for 

          System.out.println("Ingrese un Número para evaluarlo");
          numero = entrada.nextInt();

          for (int k = 0; k <= arreglo.length; k++) {

      //      System.out.println(arreglo[k]);

            if (numero == k) {

              contador++;
              posicion = arreglo[k];

            }//Fin if
            
          }//Fin del for

          System.out.printf("El Número que ingresaste se repite: %d\n",contador);
          System.out.printf("posicion: %d\n",posicion);
    
    }catch(Exception e){

          System.out.println("XXXXXXXXXXXXXXXXXXXX");
          System.out.println(" Caracter INVALIDO!");
          System.out.println("XXXXXXXXXXXXXXXXXXXX");
          ban = 1;
          entrada.nextLine();

        }//Fin Catch
        }while(ban != 0);
     
   }//Fin del método Ejercicio79
   
   public void Ejercicio80(){
     
        System.out.println("80. Elabore un programa que permita introducir un ");
        System.out.println("    arreglo de 8 elementos de tipo entero. El programa ");
        System.out.println("    mostrara un arreglo en donde muestre un 1 para los ");
        System.out.println("    primos y un 0 para los no primos.");
        System.out.println(" ");
     
        int[] entNum = new int[8];
        int[] primos = new int[8];
        Scanner entrada = new Scanner(System.in);
        
        int ban;

          do{
          try{

          ban =0;
        
          System.out.println("Dame los números para evaluar");
          
          for (int i = 0; i < entNum.length; i++) {
            
            entNum[i] = entrada.nextInt();
            
          }//Fin for
          
          for (int j = 0; j < entNum.length; j++) {
            
            int divisor = 2;
            while(entNum[j] % divisor != 0){
              
              divisor++;
              
            }//Fin del while11
            
            if (divisor == entNum[j]) {
              
              primos[j] = 1;
              
            }//Fin if
            
            else{
              
              primos[j] = 0;
              
            }//Fin else
            
          }//Fin for
          
          System.out.println("Imprimiendo Números Primos");
          for (int k = 0; k < primos.length; k++) {
            
            System.out.println(primos[k]);
            
          }//Fin del for
          
          }catch(Exception e){
    
      System.out.println("Caracter INVALIDO!");
      ban = 1;
      entrada.nextLine();
      
    }//Fin Catch
    }while(ban != 0);
     
   }//Fin del método80

}//Fin clase MetodosMiscelanea

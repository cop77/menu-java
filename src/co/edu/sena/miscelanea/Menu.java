/**
 *
 * @author Yefersson Fajardo Gonzalez
 *
 *
 */
package co.edu.sena.miscelanea;

import java.util.Scanner;

public class Menu {

  public static void main(String[] args) {

    //Entradas
    Scanner entrada = new Scanner(System.in);
    MetodosMiscelanea operadores = new MetodosMiscelanea();
    MetodosMiscelanea condicionales = new MetodosMiscelanea();
    MetodosMiscelanea ciclos = new MetodosMiscelanea();
    MetodosMiscelanea arreglos = new MetodosMiscelanea();

    int entrar = 0;
    int salir = 0;

    int opcion;
    int seleccion;
    
    byte contadorMostrar = 0;

    int ban;

    do {
      try {

        ban = 0;

        while (entrar == 0 || salir == 0) {

          System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
          System.out.println("X Bienvenido a la Miscelanea java X");
          System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX");
          System.out.println("  Por favor seleccione ");
          System.out.println("->1) 1-16  | Operadores ");
          System.out.println("->2) 17-33 | Condicionales ");
          System.out.println("->3) 34-72 | Ciclos");
          System.out.println("->4) 73-80 | Arreglos");
          System.out.println("->5) Salir");
          System.out.println("");

          opcion = entrada.nextInt();

          while (opcion <= 0 || opcion >= 6) {

            System.out.println("*************************");
            System.out.println("* ERROR! NÚMERO INVALIDO *");
            System.out.println("**************************");
            System.out.println("-> Seleccione una opción VALIDA... Por favor!");
            opcion = entrada.nextInt();

          }//Fin While

          //--
          do {
            try {

              ban = 0;

              if (opcion == 1) {

                int salirUno = 0;

                while (salirUno == 0) {

                  System.out.println("**************************");
                  System.out.println("*       OPERADORES       *");
                  System.out.println("**************************");
                  System.out.println("Seleccione un ejercicio del 1-16");
                  System.out.println("-> 0) Menú Principal");
                  System.out.println(" ");
                  
                  
                  if (contadorMostrar == 0) { //Me permite visualizar los enunciados una vez
                    
                    contadorMostrar++;
                  
                  System.out.println("1. Código java que permita calcular el área de un triángulo.");
                  System.out.println("2. Código java para introducir dos números por teclado y sumarlos.");
                  System.out.println("3. Código java para introducir un número por teclado y visualizar el                                          ");
                  System.out.println("   número elevado al cuadrado.");
                  System.out.println("4. Escribir un programa en Java que imprima por pantalla la suma de                                           ");
                  System.out.println("   1234 y 532.");
                  System.out.println("5. Escribir un programa en Java que imprima por pantalla la resta de                                           ");
                  System.out.println("   1234 y 532.");
                  System.out.println("6. Escribir un programa en Java que imprima por pantalla la ");
                  System.out.println("   multiplicación de 1234 y 532");
                  System.out.println("7. Escribir un programa en Java que imprima por pantalla la división                                          ");
                  System.out.println("   de 1234 entre 532");
                  System.out.println("8. Escribir un programa en Java que imprima por pantalla el módulo de                                          ");
                  System.out.println("   1234 entre 532");
                  System.out.println("9. Escribir un programa en Java que convierta de euros a dólares. ");
                  System.out.println("   Recibirá un número decimal correspondiente a la cantidad en euros y");                  System.out.println("   contestará con la cantidad correspondiente en dólares.");
                  System.out.println("10. Escribir un programa en Java que calcule el área de un rectángulo del ");
                  System.out.println("    cual se le proporcionará por el teclado su altura y anchura (números ");
                  System.out.println("    decimales).");
                  System.out.println("11. Escribir un algoritmo que pida el lado de un cuadrado y muestre el ");
                  System.out.println("    valor del área y del perímetro.");
                  System.out.println("12. Diseñar un algoritmo que determine el área y el volumen de un ");
                  System.out.println("    cilindro.");
                  System.out.println("13. Realizar un algoritmo que lea el radio de una circunferencia y ");
                  System.out.println("    escriba la longitud de la misma, y el área (Pi*R) ^2 del círculo ");
                  System.out.println("    inscrito.");
                  System.out.println("14. Calcular el promedio de tres números introducidos por teclado.");
                  System.out.println("15. Pidiendo el ingreso del numerador y denominador de 2 fracciones ");
                  System.out.println("    mostrar la suma.");
                  System.out.println("16. Realizar un algoritmo que calcule la potencia de un número real ");
                  System.out.println("    elevado a un número natural");
                  
                    
                  }//Fin del if para mostrar los nombres de los Ejercicios
                  
                  seleccion = entrada.nextInt();
                  
                  System.out.println("---------------------------------------------------------------------");

                  while (seleccion < 0 || seleccion >= 17) {

                    System.out.println("**************************");
                    System.out.println("* ERROR! OPCIÓN INVALIDA *");
                    System.out.println("**************************");
                    System.out.println("-> Seleccione una opción VALIDA... Por favor!");
                    seleccion = entrada.nextInt();

                  }//Fin while operadores

                  if (seleccion == 0) {

                    salirUno = 1;
                    contadorMostrar = 0;

                  }//Fin if 

                  switch (seleccion) {

                    case 1:

                      operadores.Ejercicio1();

                      break;

                    case 2:

                      operadores.Ejercicio2();

                      break;

                    case 3:

                      operadores.Ejercicio3();

                      break;

                    case 4:

                      operadores.Ejercicio4();

                      break;

                    case 5:

                      operadores.Ejercicio5();

                      break;

                    case 6:

                      operadores.Ejercicio6();

                      break;

                    case 7:

                      operadores.Ejercicio7();

                      break;

                    case 8:

                      operadores.Ejercicio8();

                      break;

                    case 9:

                      operadores.Ejercicio9();

                      break;

                    case 10:

                      operadores.Ejercicio10();

                      break;

                    case 11:

                      operadores.Ejercicio11();

                      break;

                    case 12:

                      operadores.Ejercicio12();

                      break;

                    case 13:

                      operadores.Ejercicio13();

                      break;

                    case 14:

                      operadores.Ejercicio14();

                      break;

                    case 15:

                      operadores.Ejercicio15();

                      break;

                    case 16:

                      operadores.Ejercicio16();

                      break;

                  }

                }//Fin del while salirUno

              }//Fin if opcion == 1

              //--
            } catch (Exception e) {

              System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXX");
              System.out.println("X Ups! Caracter INVALIDO! X");
              System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXX");
              System.out.println(" ");
              ban = 1;
              entrada.nextLine();

            }//Fin Catch
          } while (ban != 0);

          //-------------------- Fin de la Opción Operadores 1
          do {
            try {

              ban = 0;

              if (opcion == 2) {

                int salirDos = 0;

                while (salirDos == 0) {

                  System.out.println("***************************");
                  System.out.println("*      CONDICIONALES      *");
                  System.out.println("***************************");
                  System.out.println("Seleccione un ejercicio del 17-33");
                  System.out.println("-> 0) Menú Principal");
                  
                  if (contadorMostrar == 0){
                    
                    contadorMostrar++;
                    
                    System.out.println(" ");
                    System.out.println("17. Código para saber si un número es positivo o negativo.");
                    System.out.println("18. Escribir un programa en Java que lea dos números del teclado y diga ");                     System.out.println("    cuál es el mayor y cual es el menor.");
                    System.out.println("19. Escriba un programa que lea tres números enteros positivos, y que ");
                    System.out.println("    calcule e imprima en pantalla el menor y el mayor de todos ellos.");
                    System.out.println("20. Calcular el sueldo de los empleados de una empresa. Para ello se deberá ");
                    System.out.println("    pedir el nombre del empleado, las horas normales trabajadas y las horas ");
                    System.out.println("    extras. Tener en cuenta que el valor de la hora es de $4 y que las horas ");                System.out.println("    extras se pagan doble.");
                    System.out.println("21. Dados dos números A y B sumarlos si A es menor a B sino restarlos.");
                    System.out.println("22. Dados dos números A y B encontrar el cociente entre A y B. Recordar que ");                 
                    System.out.println("    la división por cero no está definida, en ese caso debe aparecer una ");
                    System.out.println("    leyenda anunciando que la división n es posible.");
                    System.out.println("23. numDia es una variable numérica que puede tomar 7 valores, ellos son 1, ");
                    System.out.println("    2, 3, 4, 5, 6 o 7. Mostar el nombre el nombre del día de la semana que ");
                    System.out.println("    corresponde al valor de la variable numDia.");
                    System.out.println("24. Dadas las longitudes de los tres lados de un triángulo determinar si es ");
                    System.out.println("    equilátero o no.");
                    System.out.println("25. Dados dos números A y B sumarlos si al menos uno de ellos es negativo en ");
                    System.out.println("    caso contrario multiplicarlos.");
                    System.out.println("26. Pidiendo el día y el mes de nacimiento mostrar el signo.");
                    System.out.println("27. Pidiendo el ingreso de la base y la altura de un cuadrilátero, indicar ");
                    System.out.println("    si es cuadrado o rectángulo. Para cualquiera de los dos casos mostrar el ");                System.out.println("    perímetro y la superficie de la figura.");
                    System.out.println("28. Un negocio hace descuentos en las ventas de sus productos. Si la compra ");
                    System.out.println("    es inferior a $100 el descuento es del 5%, si es mayor o igual a 100 y ");
                    System.out.println("    menor a 200 el descuento es del 10% sino será del 15%. Dado el precio de");
                    System.out.println("    la venta mostrar el descuento realizado en pesos y el precio final con ");
                    System.out.println("    descuento.");
                    System.out.println("29. Pedir el ingreso de los datos de nacimientos de un hospital: día, mes, ");
                    System.out.println("    año y sexo (1-femenino 2-masculino). Muestra el total de varones, el ");  
                    System.out.println("    total de mujeres, el total general.");
                    System.out.println("30. Se deberá pedir el sexo (F-M) y el estado civil (S-C-V-D) de las ");
                    System.out.println("    personas que lleguen a un espectáculo. Se deberán mostrar la cantidad y  ");  
                    System.out.println("    el porcentaje de hombres solteros, mujeres solteras, hombres casados,  ");  
                    System.out.println("    mujeres casadas, etc.");
                    System.out.println("31. Realizar un algoritmo que determine si un año es bisiesto o no lo es.");
                    System.out.println("32. Dados el día y mes de dos fechas, donde la primera fecha es menor a la ");
                    System.out.println("    segunda y ambas pertenece a al mismo año, calcular los días que median ");  
                    System.out.println("    entre ambas. Suponiendo que todos los meses tienen treinta días.");
                    System.out.println("33. Pidiendo la hora de ingreso y la hora de salida mostrar cuanto tiempo ");
                    System.out.println("    transcurrió. Las horas deberán pedirse como HI, MI, SI (hora de ingreso, ");  
                    System.out.println("    minuto de ingreso y segundo de ingreso) y como HS, MS, SS (hora de ");
                    System.out.println("    salida, minuto de salida y segundo de salida)");
                   
                  }//Fin del if para mostrar los nombres de los Ejercicios
                  
                  seleccion = entrada.nextInt();
                  
                  System.out.println("---------------------------------------------------------------------");
                  
                  if (seleccion == 0) {

                    salirDos = 1;
                    contadorMostrar = 0;
                    

                  }//Fin if 
                  else {

                    while (seleccion < 0 || seleccion > 0 && seleccion < 17 || seleccion > 33) {

                      System.out.println("*************************");
                      System.out.println("* ERROR! OPCIÓN INVALIDA *");
                      System.out.println("**************************");
                      System.out.println("-> Seleccione una opción VALIDA... Por favor!");
                      seleccion = entrada.nextInt();

                      if (seleccion == 0) {

                        seleccion = 0;

                      }//Fin if

                    }//Fin while operadores

                  }//Fin else

                  switch (seleccion) {

                    case 17:

                      condicionales.Ejercicio17();

                      break;

                    case 18:

                      condicionales.Ejercicio18();

                      break;

                    case 19:

                      condicionales.Ejercicio19();

                      break;

                    case 20:

                      condicionales.Ejercicio20();

                      break;

                    case 21:

                      condicionales.Ejercicio21();

                      break;

                    case 22:

                      condicionales.Ejercicio22();

                      break;

                    case 23:

                      condicionales.Ejercicio23();

                      break;

                    case 24:

                      condicionales.Ejercicio24();

                      break;

                    case 25:

                      condicionales.Ejercicio25();

                      break;

                    case 26:

                      condicionales.Ejercicio26();

                      break;

                    case 27:

                      condicionales.Ejercicio27();

                      break;

                    case 28:

                      condicionales.Ejercicio28();

                      break;

                    case 29:

                      condicionales.Ejercicio29();

                      break;

                    case 30:

                      condicionales.Ejercicio30();

                      break;

                    case 31:

                      condicionales.Ejercicio31();

                      break;

                    case 32:

                      condicionales.Ejercicio32();

                      break;

                    case 33:

                      condicionales.Ejercicio33();

                      break;

                  }//Fin switch

                }//Fin del WHILE

              }//Fin if opcion == 2

            } catch (Exception e) {

              System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXX");
              System.out.println("X Ups! Caracter INVALIDO! X");
              System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXX");
              System.out.println(" ");
              ban = 1;
              entrada.nextLine();

            }//Fin Catch
          } while (ban != 0);

          //---------------------- Fin del Opción Condicionales = 2
          do {
            try {

              ban = 0;

              if (opcion == 3) {

                int salirTres = 0;

                while (salirTres == 0) {

                  System.out.println("***************************");
                  System.out.println("*         CICLOS          *");
                  System.out.println("***************************");
                  System.out.println("Seleccione un ejercicio del 34-72");
                  System.out.println("-> 0) Menú Principal");
                  
                  if (contadorMostrar == 0){
                    
                    contadorMostrar++;
                    
                    System.out.println("");
                    System.out.println("34. Imprimir todos los múltiplos de 3 que hay entre 1 y 100.");
                    System.out.println("35. Imprimir los números impares entre 0 y 100.");
                    System.out.println("36. Imprimir los números pares del 0 al 100.");
                    System.out.println("37. Escribir un programa en Java que imprima por pantalla los números del 1 "); 
                    System.out.println("    al 3.");
                    System.out.println("38. Escribir un programa en Java que imprima por pantalla los números del 1 "); 
                    System.out.println("    al 9.");
                    System.out.println("39. Escribir un programa en Java que imprima por pantalla los números del 1 "); 
                    System.out.println("    al 10.000.");
                    System.out.println("40. Escribir un programa en Java que imprima por pantalla los números del 5 ");
                    System.out.println("    al 10.");
                    System.out.println("41. Escribir un programa en Java que imprima por pantalla los números del 5 ");
                    System.out.println("    al 15.");
                    System.out.println("42. Escribir un programa en Java que imprima por pantalla los números del 5 ");
                    System.out.println("    al 15.000.");
                    System.out.println("43. Escribir un programa en Java que imprima 200 veces la palabra “hola”.");
                    System.out.println("44. Escribir un programa en Java que imprima por pantalla los cuadrados de ");
                    System.out.println("    los números del 1 al 30.");
                    System.out.println("45. Escribir un programa en Java que multiplique los 20 primeros número ");
                    System.out.println("    naturales (1*2*3*4*5...).");
                    System.out.println("46. Escribir un programa en Java que sume los cuadrados de los cien primeros");
                    System.out.println("    números naturales, mostrando el resultado en pantalla.");
                    System.out.println("47. Escribir un programa en Java que lea un número entero desde teclado y ");
                    System.out.println("    realiza la suma de los 100 número siguientes, mostrando el resultado en ");
                    System.out.println("    pantalla. (Ejemplo: el usuario digita 5, se debe sumar 5+6+7+8+... hasta");
                    System.out.println("    que complete 100 números).");
                    System.out.println("48. Escribir un programa en Java que lea un número entero por el teclado e ");
                    System.out.println("    imprima todos los números impares menores que él.");
                    System.out.println("49. Halle el número factorial de un número ingresado por el usuario.");
                    System.out.println("50. Escriba un programa que lea temperaturas expresadas en grados Fahrenheit");
                    System.out.println("    y las convierta a grados Celsius mostrándola. El programa finalizará ");
                    System.out.println("    cuando lea un valor de temperatura igual a 999. La conversión de grados ");
                    System.out.println("    Fahrenheit (F) a Celsius (C) está dada por /C = 5/9(F - 32).");
                    System.out.println("51. Implemente una sentencia switch que escriba un mensaje en cada caso (10 ");
                    System.out.println("    opciones). Inclúyala en bucle de prueba for (5 repeticiones). Utilice ");
                    System.out.println("    también un break tras cada caso y pruébelo.");
                    System.out.println("52. Imprima los números primos hasta un número ingresado por el usuario. ");
                    System.out.println("53. Muestre por pantalla la tabla de multiplicar que el usuario desee.");
                    System.out.println("54. Construya el algoritmo que permita generar e imprimir los múltiplos de 6");
                    System.out.println("    menores que 100 ");
                    System.out.println("55. Construya el algoritmo que permita el ingreso de una serie de números y ");
                    System.out.println("    que determine cuantos números positivos, cuantos números negativos y ");                    System.out.println("    cuantos ceros hay. Como primer dato pedir la cantidad de números que ");                    System.out.println("    forman la lista.");
                    System.out.println("56. Se dispone de un conjunto de números positivos. Calcular y escribir su  ");
                    System.out.println("    promedio sabiendo que se ingresará un valor menor a 0 para indicar el  ");
                    System.out.println("    fin del conjunto de valores.");
                    System.out.println("57. Calcular el perímetro y la superficie de N rectángulos pidiendo la base ");
                    System.out.println("    y la altura.");
                    System.out.println("58. Hacer un programa que nos dé una tabla de los números factoriales de los");
                    System.out.println("    diez primeros números naturales.");
                    System.out.println("59. Hacer un programa que muestre las tablas de multiplicar del 1 al 9. Cada ");
                    System.out.println("    tabla debe tener su título.");
                    System.out.println("60. Pedir sucesivamente 20 valores numéricos. A cada valor multiplicarlo por ");
                    System.out.println("    3 y sumarle 5. Mostrar el retorno de dicha expresión junto con el número");
                    System.out.println("    que la origina. Al final mostrar el valor acumulado.");
                    System.out.println("61. Dados dos números naturales, el primero menor al segundo, generar y ");
                    System.out.println("    mostrar todos los números comprendidos entre ellos en secuencia ");
                    System.out.println("    ascendente.");
                    System.out.println("62. Diseñar un algoritmo que escriba el cubo de los números del 1 al 20.");
                    System.out.println("63. Diseñar un algoritmo que escriba el cubo de los números naturales tales ");
                    System.out.println("    que el cubo tenga como máximo cuatro cifras.");
                    System.out.println("64. Realizar un algoritmo que muestre los valores de todas las piezas del ");
                    System.out.println("    domino de forma ordenada: 0-0 0-1 1-1 0-2 1-2 2-2");
                    System.out.println("65. Realizar un algoritmo que muestre por pantalla la tabla de multiplicar ");
                    System.out.println("    del dos. Hacer tres versiones utilizando en cada una de ellas cada una ");
                    System.out.println("    de las estructuras repetitivas (for, while, do while).");
                    System.out.println("66. Realizar un algoritmo que determine el valor del cociente y el resto de ");
                    System.out.println("    una división entre números enteros ingresados por el usuario.");
                    System.out.println("67. Dada la serie de números naturales de Fibonacci: Sucesión: 1, 1, 2, 3, 5");
                    System.out.println("    , 8, 13, 21,... diseñar un algoritmo que pida un número natural y ");
                    System.out.println("    calcule e imprima la serie hasta el número ingresado.");
                    System.out.println("68. Se recibe una lista de números. Calcular y mostrar el promedio de los ");
                    System.out.println("    valores positivos y el promedio de los negativos.");
                    System.out.println("69. Sumar todos los números que se introducen mientras no sea 0. ");
                    System.out.println("70. Pedir los datos de los alumnos, estos son: sexo, edad y altura. Al final ");
                    System.out.println("    del programa se deberá mostrar la cantidad de varones, la cantidad de ");
                    System.out.println("    mujeres, la altura promedio y la cantidad de alumnos que tienen una ");
                    System.out.println("    altura mayor a 1.5 metros. El programa debe finalizar cuando la edad sea");
                    System.out.println("    igual a 0.");
                    System.out.println("71. Ingresar como datos las temperaturas registradas durante todo el día a ");
                    System.out.println("    intervalo de media hora comenzando desde la hora 0. Determinar las horas");
                    System.out.println("    en la cual se produjo la temperatura mínima y la máxima.");
                    System.out.println("72. Determinar si un número ingresado N es par o impar, controlar que el ");                    System.out.println("    número ingresado sea entero y positivo.");
                    
                    
                  }//Fin del if para mostrar los nombres de los Ejercicios
                  
                  seleccion = entrada.nextInt();
                  
                  System.out.println("---------------------------------------------------------------------");

                  if (seleccion == 0) {

                    salirTres = 1;
                    contadorMostrar = 0;

                  }//Fin if 
                  else {

                    while (seleccion < 0 || seleccion > 0 && seleccion < 34 || seleccion > 72) {

                      System.out.println("*************************");
                      System.out.println("* ERROR! OPCIÓN INVALIDA *");
                      System.out.println("**************************");
                      System.out.println("-> Seleccione una opción VALIDA... Por favor!");
                      seleccion = entrada.nextInt();

                      if (seleccion == 0) {

                        seleccion = 0;

                      }//Fin if

                    }//Fin while operadores

                  }//Fin else

                  switch (seleccion) {

                    case 34:

                      ciclos.Ejercicio34();

                      break;

                    case 35:

                      ciclos.Ejercicio35();

                      break;

                    case 36:

                      ciclos.Ejercicio36();

                      break;

                    case 37:

                      ciclos.Ejercicio37();

                      break;

                    case 38:

                      ciclos.Ejercicio38();

                      break;

                    case 39:

                      ciclos.Ejercicio39();

                      break;

                    case 40:

                      ciclos.Ejercicio40();

                      break;

                    case 41:

                      ciclos.Ejercicio41();

                      break;

                    case 42:

                      ciclos.Ejercicio42();

                      break;

                    case 43:

                      ciclos.Ejercicio43();

                      break;

                    case 44:

                      ciclos.Ejercicio44();

                      break;

                    case 45:

                      ciclos.Ejercicio45();

                      break;

                    case 46:

                      ciclos.Ejercicio46();

                      break;

                    case 47:

                      ciclos.Ejercicio47();

                      break;

                    case 48:

                      ciclos.Ejercicio48();

                      break;

                    case 49:

                      ciclos.Ejercicio49();

                      break;

                    case 50:

                      ciclos.Ejercicio50();

                      break;

                    case 51:

                      ciclos.Ejercicio51();

                      break;

                    case 52:

                      ciclos.Ejercicio52();

                      break;

                    case 53:

                      ciclos.Ejercicio53();

                      break;

                    case 54:

                      ciclos.Ejercicio54();

                      break;

                    case 55:

                      ciclos.Ejercicio55();

                      break;

                    case 56:

                      ciclos.Ejercicio56();

                      break;

                    case 57:

                      ciclos.Ejercicio57();

                      break;

                    case 58:

                      ciclos.Ejercicio58();

                      break;

                    case 59:

                      ciclos.Ejercicio59();

                      break;

                    case 60:

                      ciclos.Ejercicio60();

                      break;

                    case 61:

                      ciclos.Ejercicio61();

                      break;

                    case 62:

                      ciclos.Ejercicio62();

                      break;

                    case 63:

                      ciclos.Ejercicio63();

                      break;

                    case 64:

                      ciclos.Ejercicio64();

                      break;

                    case 65:

                      ciclos.Ejercicio65();

                      break;

                    case 66:

                      ciclos.Ejercicio66();

                      break;

                    case 67:

                      ciclos.Ejercicio67();

                      break;

                    case 68:

                      ciclos.Ejercicio68();

                      break;

                    case 69:

                      ciclos.Ejercicio69();

                      break;

                    case 70:

                      ciclos.Ejercicio70();

                      break;

                    case 71:

                      ciclos.Ejercicio71();

                      break;

                    case 72:

                      ciclos.Ejercicio72();

                      break;

                  }//Fin del switch

                }//Fin del While salirTres

              }//Fin if opcion == 3

            } catch (Exception e) {

              System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXX");
              System.out.println("X Ups! Caracter INVALIDO! X");
              System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXX");
              System.out.println(" ");
              ban = 1;
              entrada.nextLine();

            }//Fin Catch
          } while (ban != 0);

          //---------------------Fin de la Opción ciclos = 3
          do {
            try {

              ban = 0;

              if (opcion == 4) {

                int salirCuatro = 0;

                while (salirCuatro == 0) {

                  System.out.println("***************************");
                  System.out.println("*         ARREGLOS        *");
                  System.out.println("***************************");
                  System.out.println("Seleccione un ejercicio del 73-80");
                  System.out.println("-> 0) Menú Principal");
                  System.out.println(" ");
                  
                  if (contadorMostrar == 0){
                    
                    contadorMostrar++;
                    
                    System.out.println("73. Escriba un programa en java que genere aleatoriamente (Ayuda: Vea la ");
                    System.out.println("    clase Math para saber cómo generar números aleatorios en java) un array ");
                    System.out.println("    de números reales, y lo ordene mediante un método de ordenamiento ");
                    System.out.println("    (burbuja, selección o inserción) de menor a mayor. La cantidad de ");
                    System.out.println("    números será definida por el usuario.");
                    System.out.println("74. Elabore una aplicación que permita introducir 20 elementos de tipo");
                    System.out.println("    entero en un arreglo, el programa mostrara impreso el arreglo en orden ");  
                    System.out.println("    inverso.");
                    System.out.println("75. Hacer un programa que lea diez valores enteros en un array y los ");
                    System.out.println("    muestre en pantalla. después que los ordene de menor a mayor y los  ");
                    System.out.println("    vuelva a mostrar. y finalmente que los ordene de mayor a menor y los ");
                    System.out.println("    muestre por tercera vez.");
                    System.out.println("76. Elabore un programa que encuentre al número mayor y menor de un arreglo");
                    System.out.println("    y luego muestre en qué posición se encontraban estos números ");
                    System.out.println("    originalmente.");
                    System.out.println("77. Elabore un programa que imprima en orden inverso una cadena de ");
                    System.out.println("    caracteres.");
                    System.out.println("78. elabore un programa que permita introducir un arreglo de 10 elementos, ");
                    System.out.println("    el programa mostrara un histograma de esos datos (el histograma se ");
                    System.out.println("    interpretara con la salida de n asteriscos donde n es el valor de cada ");
                    System.out.println("    elemento del arreglo) ej.: el arreglo es 2,3,4 el histograma será 2->**");
                    System.out.println("    3->*** 4->****");
                    System.out.println("79. Elabore un programa que permita introducir un arreglo de 25 elementos ");
                    System.out.println("    de tipo entero. luego pedir al usuario que introduzca un número. el ");
                    System.out.println("    programa mostrara el número de veces que se repite dicho valor en el ");
                    System.out.println("    arreglo");
                    System.out.println("80. Elabore un programa que permita introducir un arreglo de 8 elementos de");
                    System.out.println("    tipo entero. El programa mostrara un arreglo en donde muestre un 1 para ");
                    System.out.println("    los primos y un 0 para los no primos.");
                    
                    
                  }//Fin del If que muestra los mensajes una sola vez! 
                  
                  
                  seleccion = entrada.nextInt();
                  
                  System.out.println("---------------------------------------------------------------------");

                  if (seleccion == 0) {

                    salirCuatro = 1;
                    contadorMostrar = 0;

                  }//Fin if 
                  else {

                    while (seleccion < 0 || seleccion > 0 && seleccion < 73 || seleccion > 80) {

                      System.out.println("*************************");
                      System.out.println("* ERROR! OPCIÓN INVALIDA *");
                      System.out.println("**************************");
                      System.out.println("-> Seleccione una opción VALIDA... Por favor!");
                      seleccion = entrada.nextInt();

                      if (seleccion == 0) {

                        seleccion = 0;

                      }//Fin if

                    }//Fin while operadores

                  }//Fin else

                  switch (seleccion) {

                    case 73:

                      arreglos.Ejercicio73();

                      break;

                    case 74:

                      arreglos.Ejercicio74();

                      break;

                    case 75:

                      arreglos.Ejercicio75();

                      break;

                    case 76:

                      arreglos.Ejercicio76();

                      break;

                    case 77:

                      arreglos.Ejercicio77();

                      break;

                    case 78:

                      arreglos.Ejercicio78();

                      break;

                    case 79:

                      arreglos.Ejercicio79();

                      break;

                    case 80:

                      arreglos.Ejercicio80();

                      break;

                  }//Fin del W

                }//Fin del While salirCuatro 

              }//Fin if opcion == 4

            } catch (Exception e) {

              System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXX");
              System.out.println("X Ups! Caracter INVALIDO! X");
              System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXX");
              System.out.println(" ");
              ban = 1;
              entrada.nextLine();

            }//Fin Catch
          } while (ban != 0);

          //----------------------Fin del Opción arreglos = 4
          if (opcion == 5) {

            entrar = 1;
            salir = 1;

          }//Fin if opcion == 5

        }//Fin while Principal del Menú

      } catch (Exception e) {

        System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXX");
        System.out.println("X Ups! Caracter INVALIDO! X");
        System.out.println("XXXXXXXXXXXXXXXXXXXXXXXXXXX");
        System.out.println(" ");
        ban = 1;
        entrada.nextLine();

      }//Fin Catch
    } while (ban != 0);

  }//Fin del main

}//Fin clase Menu
